//
//  AppDelegate.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 03/11/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IAPManager.shared.fetchProducts()
        streakCheckup()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate {
    
    func streakCheckup() {
        let currentDate = Date()
        if let lastActivityDate = CustomUserDefaults.getStreakLastDay() {
            let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: currentDate)
            if yesterday != nil && Calendar.current.isDate(yesterday!, inSameDayAs: lastActivityDate) {
                let currentStreak = CustomUserDefaults.getCurrentStreak()
                CustomUserDefaults.setCurrentStreak(data: currentStreak + 1)
            } else {
                CustomUserDefaults.setCurrentStreak(data: 1)
            }
        } else {
            CustomUserDefaults.setCurrentStreak(data: 1)
        }
        CustomUserDefaults.setStreakLastDay(data: currentDate)
    }
    
}
