//
//  MusicModel.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 01/12/23.
//

import Foundation

class MusicModel {
    var fileName = ""
    var localName = ""
    var isSelected = false
    
    init(fileName: String = "", localName: String = "", isSelected: Bool = false) {
        self.fileName = fileName
        self.localName = localName
        self.isSelected = isSelected
    }
}
