//
//  DailyHabit.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 23/11/23.
//

import Foundation
import RealmSwift

class DailyHabit: Object {
    @Persisted var breathingType: String = ""
    @Persisted var day: String = ""
    @Persisted var time: String = ""
    @Persisted var hour: String = ""
    @Persisted var minute: String = ""
    @Persisted var noon: String = ""
    @Persisted var duration: Int = 1
    @Persisted var execise: ExeciseObject?
    @Persisted(primaryKey: true) var tempId: String = ""
    
    convenience init(breathingType: String, day: String, time: String, hour: String, minute: String, noon: String, duration: Int, execise: ExeciseObject? = nil, tempId: String) {
        self.init()
        self.breathingType = breathingType
        self.day = day
        self.time = time
        self.hour = hour
        self.minute = minute
        self.noon = noon
        self.duration = duration
        self.execise = execise
        self.tempId = tempId
    }

}
