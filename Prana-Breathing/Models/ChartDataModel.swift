//
//  ChartDataModel.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 28/11/23.
//

import Foundation
import RealmSwift

class ChartDataModel: Object {
    @Persisted(primaryKey: true) var breathType = ""
    @Persisted var day = ""
    @Persisted var date = ""
    @Persisted var value = 0
    @Persisted var emotion = "goodEmoji"
    
    convenience init(breathType: String = "", day: String = "", date: String = "", value: Int = 0, emotion: String = "goodEmoji") {
        self.init()
        self.breathType = breathType
        self.day = day
        self.date = date
        self.value = value
        self.emotion = emotion
    }
}
