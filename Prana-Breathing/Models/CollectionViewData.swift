//
//  CollectionViewData.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import Foundation
import UIKit

class CollectionViewData {
    var imageName = String()
    var titleName = String()
    var isSelected = false
    
    init(imageName: String = String(), titleName: String = String() , isSelected: Bool = false) {
        self.imageName = imageName
        self.titleName = titleName
        self.isSelected = isSelected
    }
}
