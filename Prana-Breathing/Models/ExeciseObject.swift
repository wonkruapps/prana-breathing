//
//  ExeciseObject.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 23/11/23.
//

import Foundation
import RealmSwift

class ExeciseObject: Object {
    @Persisted var title = String()
    @Persisted var subTitle = String()
    @Persisted(primaryKey: true) var breathingType = String()
    @Persisted var image = String()
    @Persisted var duration = Int()
    @Persisted var inhaleSecond = Int()
    @Persisted var holdSecond = Int()
    @Persisted var exhaleSecond = Int()
    @Persisted var mainDescription = String()
    @Persisted var execiseDescription = String()
    @Persisted var scienceDescription = String()
    
    convenience init(title: String = String(), subTitle: String = String(), breathingType: String = String(), image: String = String(), duration: Int = Int(), inhaleSecond: Int = Int(), holdSecond: Int = Int(), exhaleSecond: Int = Int(), mainDescription: String = String(), execiseDescription: String = String(), scienceDescription: String = String()) {
        self.init()
        self.title = title
        self.subTitle = subTitle
        self.breathingType = breathingType
        self.image = image
        self.duration = duration
        self.inhaleSecond = inhaleSecond
        self.holdSecond = holdSecond
        self.exhaleSecond = exhaleSecond
        self.mainDescription = mainDescription
        self.execiseDescription = execiseDescription
        self.scienceDescription = scienceDescription
    }
}
