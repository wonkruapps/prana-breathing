//
//  TableData.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import Foundation

class TableData {
    
    var imageName = String()
    var year = String()
    var isSelected = false
    
    init(imageName: String = String(), year: String = String(), isSelected: Bool = false) {
        self.imageName = imageName
        self.year = year
        self.isSelected = isSelected
    }
    
}
