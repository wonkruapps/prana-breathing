//
//  ViewController.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 03/11/23.
//

import UIKit
import SafariServices
import UserNotifications

class BaseVC: UIViewController {
    
    var impactCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addNotificationObservers()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeNotificationObservers()
    }
    
    func openSafariWebView(urlString: String) {
        if let url = URL(string: urlString) {
            let controller = SFSafariViewController(url: url)
            controller.preferredBarTintColor = UIColor.primaryColor
            controller.preferredControlTintColor = UIColor.white
            controller.dismissButtonStyle = .done
            controller.configuration.barCollapsingEnabled = true
            controller.delegate = self
            present(controller, animated: true, completion: nil)
        }
    }
    
    func triggerImpactFeedback() {
        let vibration = UIImpactFeedbackGenerator(style: .heavy)
        vibration.impactOccurred()
        vibration.prepare()
        impactCount += 1
        if impactCount < 3 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.triggerImpactFeedback()
            }
        } else {
            impactCount = 0
        }
    }

    func setUpBackButton() {
        let leftBarBtn = UIBarButtonItem(image: UIImage(systemName: "chevron.backward"), style: .plain, target: self, action: #selector(actionOnDismissBtn))
        leftBarBtn.tintColor = UIColor.primaryTextColor
        navigationItem.leftBarButtonItem = leftBarBtn
    }
    
    func setupNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            if granted {
                // Schedule the notification
                CustomUserDefaults.isNotificationOn = true
                UNUserNotificationCenter.current().delegate = self
                self.scheduleNotification()
            }
        }
    }
    
    func showShareSheet(url: URL, taskName: String, initiatedView: UIView) {
        let promoText = "You’ve just been gifted a 7-Day Journey to Serenity with Nirvana’s special Guest Pass, courtesy of a friend who cares about your well-being! This isn’t just any pass – it’s your ticket to exploring the transformative power of breathing techniques, all designed to help you find your calm, enhance focus, and embrace a more mindful way of life."
//        For the share URL - we would need the app id and that would be created once we publish the app i guess"
        let activityVC = UIActivityViewController(activityItems: [promoText, url], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = initiatedView
        present(activityVC, animated: true)
    }
    
    func scheduleNotification() {
        // Create a notification content
        let content = UNMutableNotificationContent()
        content.title = "Hello!"
        content.body = "Ready to take a depth breath?"
        content.sound = UNNotificationSound.default
        
        let scheduleTime = CustomUserDefaults.getNotificatonScheduleTime()
        
        scheduleTime.forEach({
            var dateComponents = DateComponents()
            dateComponents.hour = Int(Utils.get24HourTime(time: $0)) ?? 0
            dateComponents.minute = Int(Utils.get24HourMinute(time: $0)) ?? 0
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
            
            // Create a notification request
            let request = UNNotificationRequest(identifier: NOTIFICATION_IDENTIFIER, content: content, trigger: trigger)
            
            // Add the notification request to the notification center
            UNUserNotificationCenter.current().add(request) { (error) in
                if let error = error {
                    print("Error scheduling notification: \(error.localizedDescription)")
                } else {
                    print("Notification scheduled successfully.")
                }
            }
        })
    }
    
    @objc func actionOnDismissBtn() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Notification Handler

    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.restorePurchase), name: .restorePurchase, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.inAppPurchasePurchased), name: .inAppPurchasePurchased, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.inAppPurchaseFailed), name: .inAppPurchaseFailed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.message), name: .message, object: nil)
    }
    
    func removeNotificationObservers() {
        NotificationCenter.default.removeObserver(self, name: .restorePurchase, object: nil)
        NotificationCenter.default.removeObserver(self, name: .inAppPurchasePurchased, object: nil)
        NotificationCenter.default.removeObserver(self, name: .inAppPurchaseFailed, object: nil)
        NotificationCenter.default.removeObserver(self, name: .message, object: nil)
    }
    
    @objc func restorePurchase(notification: Notification) {
        print("Restore Status")
    }

    @objc func inAppPurchasePurchased(notification: Notification) {
        print("Purchase Succeed")
    }

    @objc func inAppPurchaseFailed(notification: Notification) {
        print("Purchase Failed")
    }

    @objc func message(notification: Notification) {
        print("Message")
    }

}

// MARK: - SafaritView Delegate Method

extension BaseVC: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension BaseVC: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Display the notification inside the app
        completionHandler([.alert, .sound])
    }
}
