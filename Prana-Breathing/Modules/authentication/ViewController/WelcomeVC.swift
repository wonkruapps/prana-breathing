//
//  WelcomeVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 22/11/23.
//

import Foundation
import UIKit

class WelcomeVC: BaseVC {
    
    static let name = "WelcomeVC"
    static let storyBoard = "Welcome"
    
    class func instantiateFromStoryboard() -> WelcomeVC {
        let vc = UIStoryboard(name: WelcomeVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: WelcomeVC.name) as! WelcomeVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var attributedL: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        attributedL.attributedText = Utils.attributedText()
        cornerV.forEach({ $0.setCornerRadius(radius: 16, borderColor: Utils.theme == 0 ? UIColor.white.withAlphaComponent(0.3) : UIColor(hexStr: "6F5DDF").withAlphaComponent(0.4)) })
    }
    
    @IBAction func actionOnPop() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnLogin() {
//        let vc = LoginVC.instantiateFromStoryboard()
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnEmail() {
//        let vc = SignUpVC.instantiateFromStoryboard()
//        navigationController?.pushViewController(vc, animated: true)
    }
}
