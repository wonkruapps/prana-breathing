//
//  SignUpVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 21/11/23.
//

import Foundation
import UIKit

class SignUpVC: BaseVC {
    
    static let name = "SignUpVC"
    static let storyBoard = "SignUp"
    
    class func instantiateFromStoryboard() -> SignUpVC {
        let vc = UIStoryboard(name: SignUpVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: SignUpVC.name) as! SignUpVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var attributedL: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        attributedL.attributedText = Utils.attributedText()
        cornerV.forEach({ $0.setCornerRadius(radius: 16, borderColor: Utils.theme == 0 ? UIColor.white.withAlphaComponent(0.3) : UIColor(hexStr: "6F5DDF").withAlphaComponent(0.4)) })
    }
    
    @IBAction func actionOnLogin() {
//        let vc = LoginVC.instantiateFromStoryboard()
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnPop() {
        navigationController?.popViewController(animated: true)
    }
}
