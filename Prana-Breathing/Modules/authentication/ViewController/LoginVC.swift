//
//  File.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 22/11/23.
//

import Foundation
import UIKit

class LoginVC: BaseVC {
    
    static let name = "LoginVC"
    static let storyBoard = "Login"
    
    class func instantiateFromStoryboard() -> LoginVC {
        let vc = UIStoryboard(name: LoginVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: LoginVC.name) as! LoginVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var attributedL: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        attributedL.attributedText = Utils.attributedText()
        cornerV.forEach({ $0.setCornerRadius(radius: 16, borderColor: Utils.theme == 0 ? UIColor.white.withAlphaComponent(0.3) : UIColor(hexStr: "6F5DDF").withAlphaComponent(0.4)) })
    }
    
    @IBAction func actionOnSignIn() {
//        let vc = SignUpVC.instantiateFromStoryboard()
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnPop() {
        navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func actionOnContinue() {
//        if let errorMessage = validation() {
//            Utils.showAlert(message: errorMessage, viewController: self)
//        } else {
//            Auth.auth().signIn(withEmail: emailTF.text!.trimmingCharacters(in: .whitespaces), password: passwordTF.text!.trimmingCharacters(in: .whitespaces)) { authResult, error in
//                if let error = error {
//                    Utils.showAlert(message: error.localizedDescription, viewController: self   )
//                } else if let user = authResult?.user {
//                    CustomUserDefaults.userId = user.uid
//                    CustomUserDefaults.isLoggedIn = true
//                    let vc = PranaTabBarController.instantiateFromStoryboard()
//                    self.navigationController?.popToRootViewController(animated: true)
//                }
//            }
//        }
//    }
    
//    func validation() -> String? {
//        if emailTF.text!.isEmpty {
//            return "Please enter your email address"
//        } else if passwordTF.text!.isEmpty {
//            return "Please enter your password"
//        }
//        return nil
//    }
}
