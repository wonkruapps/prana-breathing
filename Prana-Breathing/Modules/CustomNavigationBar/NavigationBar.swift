//
//  NavigationBar.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 14/11/23.
//

import UIKit

class NavigationBar: UIView {
    
    @IBOutlet var titleL: UILabel!
    @IBOutlet var rightCornerL: UILabel!
    @IBOutlet var infoIV: UIImageView!
    @IBOutlet var backIV: UIImageView!
    @IBOutlet var navigationBar: UIView!
    
    var popViewController: (() -> Void)? /// move to previous screen Call back
    var restorePayWall: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    ///  Initialize Setup function
    required init?(coder adecoder: NSCoder) {
        super.init(coder: adecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("NavigationBar", owner: self, options: nil)
        addSubview(navigationBar)
        navigationBar.frame = bounds
        navigationBar.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func actionOnBackB() {
        popViewController?()
    }
    
    @IBAction func actionOnRestoreB() {
        restorePayWall?()
    }
    
}
