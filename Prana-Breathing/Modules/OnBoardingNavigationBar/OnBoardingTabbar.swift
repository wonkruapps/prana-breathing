//
//  OnBoardingTabbar.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 16/11/23.
//

import UIKit

class OnBoardingTabbar: UIView {
    
    @IBOutlet var progressIV: UIImageView!
    @IBOutlet var percentL: UILabel!
    @IBOutlet var middleL: UILabel!
    @IBOutlet var navigationBarV: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    ///  Initialize Setup function
    required init?(coder adecoder: NSCoder) {
        super.init(coder: adecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("OnBoardingTabbar", owner: self, options: nil)
        addSubview(navigationBarV)
        navigationBarV.frame = bounds
        navigationBarV.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
