//
//  OnBoardingSixVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 18/11/23.
//

import Foundation
import UIKit

class NotificationVC: BaseVC {
    static let name = "NotificationVC"
    static let storyBoard = "NotificationScreen"
    
    class func instantiateFromStoryboard() -> NotificationVC {
        let vc = UIStoryboard(name: NotificationVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: NotificationVC.name) as! NotificationVC
        return vc
    }
    
    @IBOutlet var switchButtons: SwitchBtn!
    @IBOutlet var cornerV: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
    }
    
    @IBAction func actionOnContinue(_ sender: Any) {
        let vc = PranaTabBarController.instantiateFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnNotificationToggle(sender: UISwitch) {
        if sender.isOn {
            let habitTime = DailyHabitDao.getAllHabits().filter({ $0.day == Date().getFormattedDay() })
            let times = habitTime.compactMap({ $0.time })
            CustomUserDefaults.setNotificatonScheduleTime(data: times)
            CustomUserDefaults.isNotificationOn = true
            setupNotification()
        } else {
            CustomUserDefaults.isNotificationOn = false
        }
    }
}
