//
//  CustomNavigationController.swift
//  GreenNoise
//
//  Created by Apzzo Technologies on 22/05/23.
//

import Foundation
import UIKit

class CustomNavigationController: UINavigationController {
    
    // MARK: - Override Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if traitCollection.userInterfaceStyle == .dark {
            Utils.theme = 1
        } else {
            Utils.theme = 0
        }
        navigationController?.navigationBar.isHidden = true
    }
}
