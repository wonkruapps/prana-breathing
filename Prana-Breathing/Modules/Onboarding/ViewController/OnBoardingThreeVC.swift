//
//  OnBoardingThird.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import Foundation
import UIKit


class OnBoardingThreeVC: BaseVC {
    static let name = "OnBoardingThreeVC"
    static let storyBoard = "OnBoardingThree"

    class func instantiateFromStoryboard() -> OnBoardingThreeVC {
        let vc = UIStoryboard(name: OnBoardingThreeVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: OnBoardingThreeVC.name) as! OnBoardingThreeVC
        return vc
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var buttonView: UIView!
    @IBOutlet var navigationBarV: OnBoardingTabbar!
    
    var tableData = [TableData]()
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        registerCell()
    }
    
    
    @IBAction func actionOnContinue(_ sender: Any) {
        let vc = OnBoardingFourVC.instantiateFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setUpView() {
        appendData()
        buttonView.isHidden = true
        navigationBarV.progressIV.image = UIImage(named: "percent60")
        navigationBarV.percentL.text = "60%"
        buttonView.setCornerRadius(radius: 16)
    }

    func appendData() {
        tableData.append(TableData(imageName: "Kid", year: "10-15 Years"))
        tableData.append(TableData(imageName: "Millennial", year: "16-24 Years"))
        tableData.append(TableData(imageName: "Adults", year: "24-40 Years"))
        tableData.append(TableData(imageName: "Older", year: "40 Years above"))
    }
    
    func registerCell() {
        let OnboardingTVC = UINib(nibName: "OnboardingTVC", bundle: nil)
        tableView.register(OnboardingTVC, forCellReuseIdentifier: "OnboardingTVC")
    }
}

extension OnBoardingThreeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OnboardingTVC", for: indexPath) as! OnboardingTVC
        let data = tableData[indexPath.row]
        cell.configCell(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = tableData[indexPath.row]
        tableData.forEach({ $0.isSelected = false})
        tableData[indexPath.row].isSelected = !data.isSelected
        buttonView.isHidden = tableData.filter({ $0.isSelected == true}).count == 0
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
}
