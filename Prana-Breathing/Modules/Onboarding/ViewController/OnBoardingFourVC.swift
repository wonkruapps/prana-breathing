//
//  OnBoardingFourVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import Foundation
import UIKit

class OnBoardingFourVC: BaseVC {
    static let name = "OnBoardingFourVC"
    static let storyBoard = "OnBoardingFour"

    class func instantiateFromStoryboard() -> OnBoardingFourVC {
        let vc = UIStoryboard(name: OnBoardingFourVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: OnBoardingFourVC.name) as! OnBoardingFourVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var buttonV: UIView!
    @IBOutlet var checkBoxIV: [UIImageView]!
    @IBOutlet var navigationBarV: OnBoardingTabbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView() {
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
        buttonV.isHidden = true
        navigationBarV.progressIV.image = UIImage(named: "percent80")
        navigationBarV.percentL.text = "80%"
    }
    
    @IBAction func actionOnContinue() {
        let vc = OnBoardingFiveVC.instantiateFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnSelection(sender: UIButton) {
        for (index,view) in checkBoxIV.enumerated() {
            if sender.tag == index {
                view.image = UIImage.fillCheckBox
                buttonV.isHidden = false
            } else {
                view.image = UIImage.emptyCheckBox
            }
        }
    }
}
