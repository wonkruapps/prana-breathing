//
//  OnBoardingFirstVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 18/11/23.
//

import Foundation
import UIKit

class OnBoardingFirstVC: BaseVC {
    static let name = "OnBoardingFirstVC"
    static let storyBoard = "OnBoardingFirst"
    
    class func instantiateFromStoryboard() -> OnBoardingFirstVC {
        let vc = UIStoryboard(name: OnBoardingFirstVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: OnBoardingFirstVC.name) as! OnBoardingFirstVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var buttonV: UIView!
    @IBOutlet var nameL: UILabel!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var navigationBarV: OnBoardingTabbar!
    
    var selectedIndex = 0
    let titleString = ["Let’s Relieve Your Stress",
                       "Reduce your anxiety",
                       "Improve you focus",
                       "Sleep better",
                       "Let's Take a Breath"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        nameL.text = "Hello \(CustomUserDefaults.userName.components(separatedBy: " ").first?.capitalized ?? "")"
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
        buttonV.isHidden = true
        buttonV.setCornerRadius(radius: 12)
        navigationBarV.progressIV.image = UIImage(named: "percent20")
        navigationBarV.percentL.text = "20%"
    }
    
    @IBAction func actionOnContinue() {
        CustomUserDefaults.setTitleString(data: titleString[selectedIndex])
        let vc = OnBoardingSecondVC.instantiateFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnSelection(sender: UIButton) {
        for (index,view) in cornerV.enumerated() {
            if sender.tag == index {
                view.backgroundColor = Utils.theme == 0 ? .black : UIColor.darkModePrimary
                buttons[index].setTitleColor(.white, for: .normal)
                buttonV.isHidden = false
                selectedIndex = sender.tag
            } else {
                buttons[index].setTitleColor(.black, for: .normal)
                view.backgroundColor = .white
            }
        }
    }
}
