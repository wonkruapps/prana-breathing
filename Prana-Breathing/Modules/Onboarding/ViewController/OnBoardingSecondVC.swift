//
//  onBoardingVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 08/11/23.
//

import Foundation
import UIKit

class OnBoardingSecondVC: UIViewController {
    static let name = "OnBoardingSecondVC"
    static let storyBoard = "OnBoardingSecond"
    
    class func instantiateFromStoryboard() -> OnBoardingSecondVC {
        let vc = UIStoryboard(name: OnBoardingSecondVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: OnBoardingSecondVC.name) as! OnBoardingSecondVC
        return vc
    }
    
    @IBOutlet var navigationBarV: OnBoardingTabbar!
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var buttonV: UIView!
    @IBOutlet var collectionView: UICollectionView!
    
    var collectionViewData = [CollectionViewData]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
        registerCell()
    }
    
    @IBAction func actionOnContinue(_ sender: Any) {
        let vc = OnBoardingThreeVC.instantiateFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setUpView() {
        buttonV.isHidden = true
        appendData()
        navigationController?.navigationBar.isHidden = true
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
        navigationBarV.percentL.text = "40%"
        navigationBarV.progressIV.image = UIImage(named: "percent40")
    }
    
    func appendData() {
        collectionViewData.append(CollectionViewData(imageName: "stressedEmoji", titleName: "Stressed"))
        collectionViewData.append(CollectionViewData(imageName: "anxiousEmoji", titleName: "Anxious"))
        collectionViewData.append(CollectionViewData(imageName: "tiredEmoji", titleName: "Tired"))
        collectionViewData.append(CollectionViewData(imageName: "happyEmoji", titleName: "Happy"))
        collectionViewData.append(CollectionViewData(imageName: "peacefulEmoji", titleName: "Peaceful"))
        collectionViewData.append(CollectionViewData(imageName: "sadEmoji", titleName: "Sad"))
    }
    
    func registerCell() {
        let OnboardingCVC = UINib(nibName: "OnboardingCVC", bundle: nil)
        collectionView.register(OnboardingCVC, forCellWithReuseIdentifier: "OnboardingCVC")
    }
}


extension OnBoardingSecondVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCVC", for: indexPath) as! OnboardingCVC
        let data = collectionViewData[indexPath.row]
        cell.configCell(collectionViewData: data)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        collectionViewData.forEach({ $0.isSelected = false })
        let data = collectionViewData[indexPath.row]
        collectionViewData[indexPath.row].isSelected = !data.isSelected
        buttonV.isHidden = collectionViewData.filter({ $0.isSelected == true }).count == 0
        collectionView.reloadData()
    }
    
    
}
