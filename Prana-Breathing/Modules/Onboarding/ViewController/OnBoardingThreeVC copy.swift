//
//  OnBoardingThird.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import Foundation
import UIKit


class OnBoardingThreeVC: BaseVC {
    static let name = "OnBoardingThreeVC"
    static let storyBoard = "OnBoardingThree"

    class func instantiateFromStoryboard() -> OnBoardingThreeVC {
        let vc = UIStoryboard(name: OnBoardingThreeVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: OnBoardingThreeVC.name) as! OnBoardingThreeVC
        return vc
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var buttonView: UIView!
    @IBOutlet var navigationBarV: OnBoardingTabbar!
    
    let tableData: [(String, String)] = [
        ("Kid", "10-15 Years"),
        ("Millennial", "16-24 Years"),
        ("Adults", "24-40 Years"),
        ("Older", "40 Years above")
    ]
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        registerCell()
    }
    
    
    @IBAction func actionOnContinue(_ sender: Any) {
        if selectedIndex != -1 {
            
        } else {
            Utils.showAlert(message: "Pick any one", viewController: self)
        }
    }
    
    func setUpView() {
        navigationBarV.progressIV.image = UIImage(named: "percent50")
        navigationBarV.percentL.text = "50%"
        buttonView.setCornerRadius(radius: 16)
    }
    
    func registerCell() {
        let OnboardingTVC = UINib(nibName: "OnboardingTVC", bundle: nil)
        tableView.register(OnboardingTVC, forCellReuseIdentifier: "OnboardingTVC")
    }
}

extension OnBoardingThreeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OnboardingTVC", for: indexPath) as! OnboardingTVC
        for (index,(key,value)) in tableData.enumerated() {
            if indexPath.row == index {
                cell.configCell(imageData: key, year: value)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OnboardingTVC
        if indexPath.row == selectedIndex {
            cell.checkBoxIV.image = UIImage(named: "emptyCheckBox")
            selectedIndex = -1
        } else if indexPath.row != selectedIndex && selectedIndex != -1 {
            let selectedCell = tableView.cellForRow(at: IndexPath(row: selectedIndex, section: 0)) as! OnboardingTVC
            selectedCell.checkBoxIV.image = UIImage(named: "emptyCheckBox")
            cell.checkBoxIV.image = UIImage(named: "fillCheckBox")
            selectedIndex = indexPath.row
        } else {
            cell.checkBoxIV.image = UIImage(named: "fillCheckBox")
            selectedIndex = indexPath.row
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
}
