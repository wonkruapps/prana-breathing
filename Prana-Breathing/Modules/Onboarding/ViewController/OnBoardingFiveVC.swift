//
//  OnBoardingFiveVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import Foundation
import UIKit
import JKCircularProgess

class OnBoardingFiveVC: BaseVC {
    static let name = "OnBoardingFiveVC"
    static let storyBoard = "OnBoardingFive"
    
    class func instantiateFromStoryboard() -> OnBoardingFiveVC {
        let vc = UIStoryboard(name: OnBoardingFiveVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: OnBoardingFiveVC.name) as! OnBoardingFiveVC
        return vc
    }
    
    @IBOutlet var navigationBarV: OnBoardingTabbar!
    @IBOutlet var gif1IV: UIImageView!
    @IBOutlet var gif2IV: UIImageView!
    @IBOutlet var gif3IV: UIImageView!
    @IBOutlet var gifIVs: [UIImageView]!
    
    var timer: Timer?
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(updateImage), userInfo: nil, repeats: true)
    }
    
    func setUpView() {
        navigationBarV.middleL.text = "Almost there..."
        navigationBarV.progressIV.image = UIImage(named: "AlmostThere")
        gifIVs.forEach({ $0.image = UIImage.gif(name: "Loader")})
    }
    
    @objc func updateImage() {
        guard count < gifIVs.count else {
            timer?.invalidate()
            let vc = PayWallVC.instantiateFromStoryboard()
            vc.modalPresentationStyle = .overFullScreen
            present(vc, animated: true)
            return
        }
        gifIVs[count].image = UIImage(named: "onboardingTick")
        count += 1
    }
}
