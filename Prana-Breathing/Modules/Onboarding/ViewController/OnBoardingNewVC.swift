//
//  OnBoardingNewVc.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 25/11/23.
//

import Foundation
import UIKit

class OnBoardingNewVC: BaseVC {
    static let name = "OnBoardingNewVC"
    static let storyBoard = "OnBoardingNew"
    
    class func instantiateFromStoryboard() -> OnBoardingNewVC {
        let vc = UIStoryboard(name: OnBoardingNewVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: OnBoardingNewVC.name) as! OnBoardingNewVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var nameTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
    }
    
    @IBAction func actionOnContinue() {
        if let errorMessage = validation() {
            Utils.showAlert(message: errorMessage, viewController:  self)
        } else {
            CustomUserDefaults.userName = nameTF.text?.trimmingCharacters(in: .whitespaces) ?? ""
            let vc = OnBoardingFirstVC.instantiateFromStoryboard()
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func validation() -> String? {
        if nameTF.text!.isEmpty {
            return "Please enter your name"
        }
        return nil
    }
}

extension OnBoardingNewVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // Hide the keyboard
        return true
    }
}
 
