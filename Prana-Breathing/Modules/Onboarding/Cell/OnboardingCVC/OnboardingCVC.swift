//
//  OnboardingCVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 16/11/23.
//

import UIKit

class OnboardingCVC: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        view.setCornerRadius(radius: 16)
    }
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var view: UIView!
    
    func configCell(collectionViewData: CollectionViewData) {
        if collectionViewData.isSelected {
            view.backgroundColor = Utils.theme == 0 ? UIColor(hexStr: "7272FF").withAlphaComponent(0.8) : UIColor(hexStr: "6F5DDF")
        } else {
            view.backgroundColor = Utils.theme == 0 ? UIColor(hexStr: "DBDBFF").withAlphaComponent(0.8) : UIColor(hexStr: "6F5DDF").withAlphaComponent(0.3)
        }
        imageView.image = UIImage(named: collectionViewData.imageName)
        label.text = collectionViewData.titleName
    }
}
