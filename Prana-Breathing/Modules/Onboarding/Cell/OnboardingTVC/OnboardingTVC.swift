//
//  OnboardingTVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import UIKit

class OnboardingTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        cornerV.setCornerRadius(radius: 16)
    }
    
    @IBOutlet var cornerV: UIView!
    @IBOutlet var profileIV: UIImageView!
    @IBOutlet var checkBoxIV: UIImageView!
    @IBOutlet var yearL: UILabel!
    @IBOutlet var profileL: UILabel!
    
    
    func configCell(data: TableData) {
        checkBoxIV.image = data.isSelected ? UIImage.fillCheckBox : UIImage.emptyCheckBox
        profileIV.image = UIImage(named: data.imageName)
        profileL.text = data.imageName
        yearL.text = data.year
    }
}
