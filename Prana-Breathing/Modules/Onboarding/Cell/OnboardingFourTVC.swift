//
//  OnboardingFourTVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 17/11/23.
//

import UIKit

class OnboardingFourTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBOutlet var contentIV: UIImageView!
    @IBOutlet var checkBoxIV: UIImageView!
    @IBOutlet var titleL: UILabel!
    
}
