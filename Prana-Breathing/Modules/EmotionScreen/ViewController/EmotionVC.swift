//
//  EmotionVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 06/12/23.
//

import Foundation
import UIKit

class EmotionVC: BaseVC {
    static let name = "EmotionVC"
    static let storyBoard = "EmotionScreen"
    
    class func instantiateFromStoryboard() -> EmotionVC {
        let vc = UIStoryboard(name: EmotionVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: EmotionVC.name) as! EmotionVC
        return vc
    }
    
    @IBOutlet var buttonV: UIView!
    @IBOutlet var navigationBarV: NavigationBar!
    @IBOutlet var emojiLs: [UILabel]!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        buttonV.setCornerRadius(radius: 16)
        navigationBarV.infoIV.image = UIImage(systemName: "xMark")
        navigationBarV.restorePayWall = {
            Utils.setRootVC(.homeVC)
        }
    }
    
    @IBAction func actionOnSelection(sender: UIButton) {
        let chartData = ChartDataDao.getAllChartData().filter({ $0.day == Date().getFormattedDay() })
        switch sender.tag {
        case 0:
            ChartDataDao.saveOrUpdateEmoji(chartData: chartData, emoji: Emoji.Awful.rawValue)
        case 1:
            ChartDataDao.saveOrUpdateEmoji(chartData: chartData, emoji: Emoji.Bad.rawValue)
        case 2:
            ChartDataDao.saveOrUpdateEmoji(chartData: chartData, emoji: Emoji.Meh.rawValue)
        case 3:
            ChartDataDao.saveOrUpdateEmoji(chartData: chartData, emoji: Emoji.Good.rawValue)
        case 4:
            ChartDataDao.saveOrUpdateEmoji(chartData: chartData, emoji: Emoji.Great.rawValue)
        default:
            break 
        }
        emojiLs.forEach({ $0.textColor = .white })
        emojiLs[sender.tag].textColor = UIColor.darkModePrimary
    }
    
    @IBAction func actionOnSubmit() {
        Utils.setRootVC(.homeVC)
    }
    
}
