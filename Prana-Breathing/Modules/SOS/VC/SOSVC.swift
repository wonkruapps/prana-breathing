//
//  SOSVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies on 27/11/23.
//

import UIKit

class SOSVC: UIViewController {
    
    static let name = "SOSVC"
    static let storyBoard = "SOS"
    
    class func instantiateFromStoryboard() -> SOSVC {
        let vc = UIStoryboard(name: SOSVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: SOSVC.name) as! SOSVC
        return vc
    }
    
    @IBOutlet var checkBoxIV: [UIImageView]!
    @IBOutlet var buttonV: UIView!
    
    var selectedIndex = 0 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView() {
        buttonV.setCornerRadius(radius: 16)
        buttonV.isHidden = true
    }
    
    @IBAction func actionOnSelection(sender: UIButton) {
        selectedIndex = sender.tag
        for (index,view) in checkBoxIV.enumerated() {
            if sender.tag == index {
                view.image = UIImage.sosFillCheckBox
                buttonV.isHidden = false
            } else {
                view.image = UIImage.sosEmptyCheckBox
            }
        }
    }
    
    @IBAction func actionOnPop(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnContinue(){
        let vc = RelaxVC.instantiateFromStoryboard()
        switch selectedIndex {
        case 0 :
            vc.execise = ExeciseDao.getSingleExecise(breathType: "Balance")
        case 1 :
            vc.execise = ExeciseDao.getSingleExecise(breathType: "Energize")
        case 2 :
            vc.execise = ExeciseDao.getSingleExecise(breathType: "Focus")
        case 3 :
            vc.execise = ExeciseDao.getSingleExecise(breathType: "Calm")
        case 4 :
            vc.execise = ExeciseDao.getSingleExecise(breathType: "Strengthen")
        case 5 :
            vc.execise = ExeciseDao.getSingleExecise(breathType: "Relax")
        default :
            break
        }
        vc.isFromSos = true
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
