//
//  BreathTechniquesVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 07/11/23.
//

import Foundation
import UIKit

class BreathTechniquesVC: BaseVC {
    
    static let name = "BreathTechniquesVC"
    static let storyBoard = "BreathTechniques"
    
    class func instantiateFromStoryboard() -> BreathTechniquesVC {
        let vc = UIStoryboard(name: BreathTechniquesVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: BreathTechniquesVC.name) as! BreathTechniquesVC
        return vc
    }
    
    @IBOutlet var collectionView: UICollectionView!
    
    var execises = [ExeciseObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
    }

    
    func registerCell() {
        let breathTechniquesCVC = UINib(nibName: "BreathTechniquesCVC", bundle: nil)
        collectionView.register(breathTechniquesCVC, forCellWithReuseIdentifier: "BreathTechniquesCVC")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        execises = ExeciseDao.getAllExecise().filter({ $0.breathingType != ExeciseType.PacedBreathing.rawValue})
        collectionView.reloadData()
        navigationController?.navigationBar.isHidden = true
    }
}

extension BreathTechniquesVC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return execises.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BreathTechniquesCVC", for: indexPath) as! BreathTechniquesCVC
        cell.configCell(element: execises[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = RelaxVC.instantiateFromStoryboard()
        vc.execise = execises[indexPath.row]
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let device = UIDevice.current
        return device.model == "iPad" ? CGSize(width: 340, height: 490) : CGSize(width: 170, height: 245)
    }
    
}
