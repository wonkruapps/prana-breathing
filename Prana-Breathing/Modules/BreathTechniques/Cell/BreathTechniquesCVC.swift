//
//  BreathTechniquesCVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 21/11/23.
//

import UIKit

class BreathTechniquesCVC: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        cornerV.setCornerRadius(radius: 20)
        // Initialization code
    }
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var cornerV: UIView!
    @IBOutlet var titleL: UILabel!
    @IBOutlet var subTitleL: UILabel!
    
    func configCell(element: ExeciseObject) {
        titleL.text = element.title
        subTitleL.text = element.subTitle
        imageView.image = UIImage(named: element.image)
    }

}
