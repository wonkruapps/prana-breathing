//
//  ReasonVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 18/11/23.
//

import Foundation
import UIKit

class ReasonVC: BaseVC {
    
    static let name = "ReasonVC"
    static let storyBoard = "Reason"
    
    class func instantiateFromStoryboard() -> ReasonVC {
        let vc = UIStoryboard(name: ReasonVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: ReasonVC.name) as! ReasonVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var checkBoxIV: [UIImageView]!
    @IBOutlet var buttonV: UIView!
    @IBOutlet var navigationBarV: NavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView(){
        navigationBarV.backIV.image = UIImage(systemName: "xmark")?.withTintColor(.white)
        navigationBarV.infoIV.isHidden = true
        buttonV.isHidden = true
        buttonV.setCornerRadius(radius: 12)
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
    }
    
    @IBAction func actionOnSelection(sender: UIButton) {
        for (index,view) in checkBoxIV.enumerated() {
            if sender.tag == index {
                view.image = UIImage.fillCheckBox
                buttonV.isHidden = false
            } else {
                view.image = UIImage.emptyCheckBox
            }
        }
    }
    
    @IBAction func actionOnContinue() {
        let vc = PayWallVC.instantiateFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
