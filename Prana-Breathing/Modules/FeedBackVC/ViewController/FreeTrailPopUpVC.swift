//
//  FreeTrailPopUpVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 20/11/23.
//

import Foundation
import UIKit

class FreeTrailPopUpVC: BaseVC {
    
    static let name = "FreeTrailPopUpVC"
    static let storyBoard = "FreeTrailPopUp"
    
    class func instantiateFromStoryboard() -> FreeTrailPopUpVC {
        let vc = UIStoryboard(name: FreeTrailPopUpVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: FreeTrailPopUpVC.name) as! FreeTrailPopUpVC
        return vc
    }
    
    @IBOutlet var cornerV: UIView!
    @IBOutlet var attributeL: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cornerV.setCornerRadius(radius: 16)
        let attributedString = NSMutableAttributedString(string: attributeL.text!)
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor : UIColor(hexStr: "6F5DDF")], range: NSRange(location: 106, length: 10))
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor : UIColor(hexStr: "6F5DDF")], range: NSRange(location: 120, length: 7))
        attributeL.attributedText = attributedString
    }
    
    @IBAction func actionOnPop() {
        dismiss(animated: true)
    }
}
