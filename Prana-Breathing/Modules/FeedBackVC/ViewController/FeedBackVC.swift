//
//  FeedBackVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 09/11/23.
//

import Foundation
import UIKit


class FeedBackVC: BaseVC {
    
    static let name = "FeedBackVC"
    static let storyBoard = "FeedBackScreen"
    
    class func instantiateFromStoryboard() -> FeedBackVC {
        let vc = UIStoryboard(name: FeedBackVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: FeedBackVC.name) as! FeedBackVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpTitle(title: "")
        setUpNavigationButton()
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
    }
    
    func setUpNavigationButton() {
        let leftBarBtn = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .plain, target: self, action: #selector(actionOnDismiss))
        leftBarBtn.tintColor = UIColor.primaryTextColor
        navigationItem.leftBarButtonItem = leftBarBtn
    }
    
    @objc func actionOnDismiss() {
        if CustomUserDefaults.getLastActiveDay() != Date() {
            CustomUserDefaults.setLastActiveDay(data: Date())
            let vc = EmotionVC.instantiateFromStoryboard()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actionOnRepeat() {
        if CustomUserDefaults.getLastActiveDay() != Date() {
            CustomUserDefaults.setLastActiveDay(data: Date())
            let vc = EmotionVC.instantiateFromStoryboard()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actionOnGuestPass() {
        let vc = FreeTrailVC.instantiateFromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
}
