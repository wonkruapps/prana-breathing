//
//  FreeTrailVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 18/11/23.
//

import Foundation
import UIKit

class FreeTrailVC: BaseVC {
    
    static let name = "FreeTrailVC"
    static let storyBoard = "FreeTrail"
    
    class func instantiateFromStoryboard() -> FreeTrailVC {
        let vc = UIStoryboard(name: FreeTrailVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: FreeTrailVC.name) as! FreeTrailVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var navigationBarV: NavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    @IBAction func actionOnShare(_ sender: Any) {
        showShareSheet(url: URL(fileURLWithPath: ""), taskName: "", initiatedView: self.view)
    }
    @IBAction func actionOnMessage(_ sender: Any) {
        let recipientPhoneNumber = "1234567890"
        let messageContent = "Hello, this is a pre-filled message!"
        if let url = URL(string: "sms:\(recipientPhoneNumber)&body=\(messageContent.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func setUpView(){
        navigationController?.navigationBar.isHidden = true
        navigationBarV.backIV.image = UIImage(systemName: "xmark")?.withTintColor(.white)
        navigationBarV.infoIV.isHidden = true
        navigationBarV.popViewController = {
            let vc = ReasonVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        cornerV.forEach({ $0.setCornerRadius(radius: 16) })
    }
}
