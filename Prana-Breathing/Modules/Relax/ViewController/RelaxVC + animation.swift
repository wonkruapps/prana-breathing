//
//  RelaxVC + animation.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 27/11/23.
//

import Foundation
import UIKit
import Lottie

extension RelaxVC {
    
    func setUpCircleAnimation() {
        // Create a Circle Animation
        animationView.loopMode = .playOnce
        animationView.contentMode = .scaleToFill
        animationView.frame = CGRect(x: 0, y: 0, width: 230, height: 230)
        animationV.addSubview(animationView)
        
        // Create a UILabel
        label.text = "Inhale"
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(name: "Inter-Bold", size: 18)
        label.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        label.center = animationView.center
        animationV.addSubview(label)
        
    }
    
    func startCircleAnimation() {
        label.text = "Inhale"
        holdSecond = execise?.holdSecond ?? 0
        switch execise?.breathingType {
        case "Relax":
            animationView.animationSpeed = 0.3
            playAnimation(from: 0, to: 1) {
                self.label.text = "Hold"
                // Animation completion block - called when the inhale animation is finished
                // Pause for 4 seconds
                if !self.isAnimationPaused {
                    self.pauseFor(duration: TimeInterval(self.holdSecond)) {
                        // Play the exhale animation for 4 seconds
                        self.label.text = "Exhale"
                        self.animationView.animationSpeed = 0.13
                        self.playAnimation(from: 1, to: 0) {
                            // Animation completion block - called when the exhale animation is finished
                            self.startCircleAnimation()
                        }
                    }
                }
            }
        case "Calm":
            animationView.animationSpeed = 0.3
            playAnimation(from: 0, to: 1) {
                self.label.text = "Hold"
                // Animation completion block - called when the inhale animation is finished
                // Pause for 4 seconds
                if !self.isAnimationPaused {
                    self.pauseFor(duration: TimeInterval(self.holdSecond)) {
                        // Play the exhale animation for 4 seconds
                        self.label.text = "Exhale"
                        self.animationView.animationSpeed = 0.15
                        self.playAnimation(from: 1, to: 0) {
                            // Animation completion block - called when the exhale animation is finished
                            self.startCircleAnimation()
                        }
                    }
                }
            }
        case "Balance":
            animationView.animationSpeed = 0.3
            playAnimation(from: 0, to: 1) {
                self.label.text = "Hold"
                // Animation completion block - called when the inhale animation is finished
                // Pause for 4 seconds
                if !self.isAnimationPaused {
                    self.pauseFor(duration: TimeInterval(self.holdSecond)) {
                        // Play the exhale animation for 4 seconds
                        self.label.text = "Exhale"
                        self.playAnimation(from: 1, to: 0) {
                            // Animation completion block - called when the exhale animation is finished
                            self.startCircleAnimation()
                        }
                    }
                }
            }
        case "Strengthen":
            animationView.animationSpeed = 0.7
            playAnimation(from: 0, to: 1) {
                // Animation completion block - called when the inhale animation is finished
                // Play the exhale animation for 4 seconds
                if !self.isAnimationPaused {
                    self.label.text = "Exhale"
                    self.playAnimation(from: 1, to: 0) {
                        // Animation completion block - called when the exhale animation is finished
                        self.startCircleAnimation()
                    }
                }
            }
        case "Focus":
            animationView.animationSpeed = 0.3
            playAnimation(from: 0, to: 1) {
                self.label.text = "Hold"
                // Animation completion block - called when the inhale animation is finished
                // Pause for 4 seconds
                if !self.isAnimationPaused {
                    self.pauseFor(duration: TimeInterval(self.holdSecond)) {
                        // Play the exhale animation for 4 seconds
                        self.label.text = "Exhale"
                        self.animationView.animationSpeed = 0.16
                        self.playAnimation(from: 1, to: 0) {
                            // Animation completion block - called when the exhale animation is finished
                            self.startCircleAnimation()
                        }
                    }
                }
            }
        case "Energise":
            animationView.animationSpeed = 0.6
            playAnimation(from: 0, to: 1) {
                self.label.text = "Hold"
                // Animation completion block - called when the inhale animation is finished
                // Pause for 4 seconds
                if !self.isAnimationPaused {
                    self.pauseFor(duration: TimeInterval(self.holdSecond)) {
                        // Play the exhale animation for 4 seconds
                        self.label.text = "Exhale"
                        self.animationView.animationSpeed = 0.3
                        self.playAnimation(from: 1, to: 0) {
                            // Animation completion block - called when the exhale animation is finished
                            self.startCircleAnimation()
                        }
                    }
                }
            }
            
        case "PacedBreathing":
            animationView.animationSpeed = 0.3
            playAnimation(from: 0, to: 1) {
                self.label.text = "Hold"
                // Animation completion block - called when the inhale animation is finished
                // Pause for 4 seconds
                if !self.isAnimationPaused {
                    self.pauseFor(duration: TimeInterval(self.holdSecond)) {
                        // Play the exhale animation for 4 seconds
                        self.label.text = "Exhale"
                        self.animationView.animationSpeed = 0.2
                        self.playAnimation(from: 1, to: 0) {
                            // Animation completion block - called when the exhale animation is finished
                            self.startCircleAnimation()
                        }
                    }
                }
            }
            
        default:
            break
        }
    }
    
    func startSliderAnimation() {
        sliderL.text = "Inhale"
        holdSecond = execise?.holdSecond ?? 0
        sliderAnimationV.endPointValue = 0
        startSliderTimer(duration: TimeInterval(execise?.inhaleSecond ?? 1))
    }
    
    
    
    /// PLAY ANIMATION WITH SPECIFIED FUNCTIONALITY
    /// - Parameters:
    ///   - startProgress: START POINT OF FRAME
    ///   - endProgress: END POINT OF FRAME
    func playAnimation(from startProgress: AnimationProgressTime, to endProgress: AnimationProgressTime, completion: @escaping () -> Void) {
        animationView.play(fromProgress: startProgress, toProgress: endProgress, loopMode: .none) { _ in
            if CustomUserDefaults.isVibrationOn {
                self.triggerImpactFeedback()
            }
            completion()
        }
    }
    
    /// PASUE THE ANIMATION FOR HOLD FUNCTIONALITY
    /// - Parameters:
    ///   - duration: NUMBER OF SECOND TO HOLD THE ANIMATION IN PAUSE STATE
    func pauseFor(duration: TimeInterval, completion: @escaping () -> Void) {
        sucideTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(didExeciseStop), userInfo: nil, repeats: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            if self.sucideTimer?.isValid ?? true {
                self.sucideTimer?.invalidate()
                if CustomUserDefaults.isVibrationOn {
                    self.triggerImpactFeedback()
                }
                completion()
            }
        }
    }
    
    @objc func didExeciseStop() {
        if isAnimationPaused {
            stopTimer()
        }
    }
    
    /// STOP THE ANIMATION
    func stopAnimation() {
        isAnimationPaused = true
        animationView.stop()
    }
}
