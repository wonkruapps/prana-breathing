//
//  RelaxVC + timer.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 27/11/23.
//

import Foundation

extension RelaxVC {
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateMainSecond), userInfo: nil, repeats: true)
    }
    
    func startSliderTimer(duration: TimeInterval) {
        sliderTimer = Timer.scheduledTimer(timeInterval: duration / 100, target: self, selector: #selector(updateSliderPosition), userInfo: nil, repeats: true)
    }
    
    func startExhaleSliderTimer(duration: TimeInterval) {
        labelTimer = Timer.scheduledTimer(timeInterval: duration / 100, target: self, selector: #selector(updateSliderPositionForExhale), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateMainSecond() {
        guard secondCount > 0 else {
            stopTimer()
            stopAnimation()
            startBtn.setTitle("Start", for: .normal)
            if isFromTask {
                let chartData = ChartDataModel(breathType: execise?.breathingType ?? "", day: Date().getFormattedDay(), date: Date().getFormattedDate(), value: 1)
                ChartDataDao.saveOrUpdateUser(chartData: chartData)
                if DailyHabitDao.getAllHabits().count != 0 {
                    guard let habit = DailyHabitDao.getAllHabits().filter({ $0.breathingType == execise?.breathingType && $0.day == Date().getFormattedDay() }).first else { return }
                    DailyHabitDao.taskStatusChange(habit: habit)
                }
            }
            CustomUserDefaults.setTotalMinute(data: CustomUserDefaults.getTotalMinute() + (execise?.duration ?? 1))
            CustomUserDefaults.setTotalSession(data: CustomUserDefaults.getTotalSession() + 1)
            let vc = FeedBackVC.instantiateFromStoryboard()
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        secondCount -= 1
        setCountDown()
    }
    
    @objc func updateSliderPosition() {
        if sliderAnimationV.maximumValue > sliderAnimationV.endPointValue {
            sliderAnimationV.endPointValue  += 1
        } else {
            sliderTimer?.invalidate()
            sliderL.text = "Hold"
            pauseFor(duration: TimeInterval(holdSecond)) {
                self.sliderAnimationV.endPointValue = 0
                self.sliderL.text = "Exhale"
                self.startExhaleSliderTimer(duration: TimeInterval(self.execise?.exhaleSecond ?? 1))
            }
        }
    }
    
    @objc func updateSliderPositionForExhale() {
        if sliderAnimationV.maximumValue > sliderAnimationV.endPointValue {
            sliderAnimationV.endPointValue  += 1
        } else {
            labelTimer?.invalidate()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.startSliderAnimation()
            }
        }
    }
    
    func setCountDown() {
        let minutes = Int(secondCount) / 60
        let seconds = Int(secondCount) % 60
        let formattedTime = String(format: "%02d:%02d", minutes, seconds)
        countDownL.text = formattedTime
    }
    
    func stopTimer() {
        audioPlayer = nil
        sliderL.text = "Inhale"
        label.text = "Inhale"
        secondCount = (execise?.duration ?? 1) * 60
        setCountDown()
        sliderAnimationV.endPointValue = 0
        sliderTimer?.invalidate()
        labelTimer?.invalidate()
        timer?.invalidate()
        sucideTimer?.invalidate()
    }
}
