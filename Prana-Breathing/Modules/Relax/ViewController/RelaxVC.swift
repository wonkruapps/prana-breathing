//
//  RelaxVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 07/11/23.
//

import Foundation
import AVFoundation
import UIKit
import Lottie
import HGCircularSlider

class RelaxVC: BaseVC {
    
    static let name = "RelaxVC"
    static let storyBoard = "RelaxScreen"
    
    class func instantiateFromStoryboard() -> RelaxVC {
        let vc = UIStoryboard(name: RelaxVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: RelaxVC.name) as! RelaxVC
        return vc
    }
    
    @IBOutlet var animationV: UIView!
    @IBOutlet var buttonStackV: UIStackView!
    @IBOutlet var sliderAnimationV: CircularSlider!
    @IBOutlet var navigationBarV: NavigationBar!
    @IBOutlet var buttonViews: [UIView]!
    @IBOutlet var animationBackgroundV: UIView!
    @IBOutlet var startBtn: UIButton!
    @IBOutlet var subTitleL: UILabel!
    @IBOutlet var sliderL: UILabel!
    @IBOutlet var countDownL: UILabel!
    
    var isStarted = false
    var isAnimationPaused = false
    var isFromTask = false
    var isFromSos = false
    var execise: ExeciseObject?
    var secondCount = 0
    var holdSecond = 0
    var timer: Timer?
    var labelTimer: Timer?
    var sliderTimer: Timer?
    var sucideTimer: Timer?
    let label = UILabel()
    var animationView = LottieAnimationView()
    var audioPlayer : AVAudioPlayer?
    let vibration = UIImpactFeedbackGenerator(style: .medium)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpViews()
    }
    
    @IBAction func actionOnInfoPage(_ sender: Any) {
        stopTimer()
        stopAnimation()
        let vc = InfoVC.instantiateFromStoryboard()
        vc.execise = execise
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnHabitChange(_ sender: Any) {
        stopTimer()
        stopAnimation()
        let habitVC = HabitVC.instantiateFromStoryboard()
        habitVC.execise = execise
        habitVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(habitVC, animated: true)
    }
    
    @IBAction func actionOnfilter(_ sender: Any) {
        stopTimer()
        stopAnimation()
        let filterVC = FilterVC.instantiateFromStoryboard()
        filterVC.execise = execise
        navigationController?.pushViewController(filterVC, animated: true)
    }
    
    @IBAction func actionOnStart(sender: UIButton) {
        isStarted.toggle()
        if isStarted {
            isAnimationPaused = false
            if CustomUserDefaults.isBackgroundMusicOn {
                playAudio(music: CustomUserDefaults.getBackgroundMusic() ?? "")
            }
            startBtn.setTitle("Stop", for: .normal)
            CustomUserDefaults.animationType == "SliderAnimation" ? startSliderAnimation() : startCircleAnimation()
            startTimer()
        } else {
            startBtn.setTitle("Start", for: .normal)
            stopAnimation()
            holdSecond = 0
            secondCount = 0
            stopTimer()
        }
    }
    
    func setUpViews() {
        if execise?.breathingType == ExeciseType.PacedBreathing.rawValue {
            buttonViews[0].isHidden = true
        } else {
            buttonViews[0].isHidden = false
        }
        buttonStackV.isHidden = isFromSos
        secondCount = (execise?.duration ?? 1) * 60
        setCountDown()
        if CustomUserDefaults.animationType == "SliderAnimation" {
            animationV.isHidden = true
            sliderAnimationV.isHidden = false
        } else {
            animationView = LottieAnimationView(animation: LottieAnimation.named( Utils.theme == 0 ? "BreathingAnimationLight" : "BreathingAnimationDark" ))
            sliderAnimationV.isHidden = true
            animationV.isHidden = false
            setUpCircleAnimation()
        }
        sliderL.setCornerRadius(radius: 45)
        setUpNavigationBar()
        startBtn.setCornerRadius(radius: 16)
        buttonViews.forEach({ $0.setCornerRadius(radius: 26) })
        animationBackgroundV.setCornerRadius(radius: 105)
    }
    
    func setUpNavigationBar() {
        navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.tintColor = Utils.theme == 0 ? .black : .white
        navigationBarV.infoIV.isHidden = true
        navigationBarV.titleL.text = isFromSos ? "SOS" : execise?.title
        subTitleL.text = execise?.subTitle
        navigationBarV.popViewController = {
            self.stopAnimation()
            self.stopTimer()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func playAudio(music: String) {
        guard let audioPath = Bundle.main.path(forResource: music, ofType: "mp3") else {
            print("Audio file not found")
            return
        }

        let audioURL = URL(fileURLWithPath: audioPath)

        do {
            audioPlayer = try AVAudioPlayer(contentsOf: audioURL)
            audioPlayer?.numberOfLoops = -1
            audioPlayer?.play()
        } catch {
            print("Error loading audio file: \(error.localizedDescription)")
        }
    }
    
}
