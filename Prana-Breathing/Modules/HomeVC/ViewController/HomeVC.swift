//
//  HomeVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 16/11/23.
//

import Foundation
import UIKit
import Lottie

class HomeVC: BaseVC {
    
    static let name = "HomeVC"
    static let storyBoard = "HomeScreen"
    
    class func instantiateFromStoryboard() -> HomeVC {
        let vc = UIStoryboard(name: HomeVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: HomeVC.name) as! HomeVC
        return vc
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var sosV: UIView!
    @IBOutlet var headerV: UIView!
    @IBOutlet var premiumV: UIView!
    @IBOutlet var headerL: UILabel!
    @IBOutlet var TitleL: UILabel!
    @IBOutlet var nameL: UILabel!
    @IBOutlet var animationIV: UIImageView!
    @IBOutlet var noOfPeopleL: UILabel!
    @IBOutlet var tableHCONS: NSLayoutConstraint!
    
    var data = [DailyHabit]()
    var execises = [ExeciseObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        CustomUserDefaults.isOnboardFinished = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpViews()
        setUpRotateImage()
    }
    
    func registerCell() {
        let homeTVC = UINib(nibName: "HomeTVC", bundle: nil)
        tableView.register(homeTVC, forCellReuseIdentifier: "HomeTVC")
    }
    
    func setUpViews() {
        data = DailyHabitDao.getAllHabits().filter({ $0.day == Date().getFormattedDay() })
        if data.isEmpty {
            let localExecise = ExeciseDao.getAllExecise().filter({ $0.breathingType == "Balance" || $0.breathingType == "Relax" })
            if localExecise.isEmpty {
                appendData()
            } else {
                execises = localExecise
            }
        }
        premiumV.isHidden = Utils.isSubscribed
        tableHCONS.constant = CGFloat(data.isEmpty ? (execises.count * 130) + 40 : (data.count * 130) + 40)
        headerL.text = data.isEmpty ? "Task" : "Today"
        nameL.text = "Hello \(CustomUserDefaults.userName.components(separatedBy: " ").first?.capitalized ?? "")"
        TitleL.text = CustomUserDefaults.getTitleString()
        navigationController?.navigationBar.isHidden = true
        let randomNumber = Int.random(in: 80000...110000)
        noOfPeopleL.text = "\(randomNumber) people practiced today!"
        tableView.tableHeaderView = headerV
        sosV.setCornerRadius(radius: 16)
        tableView.reloadData()
    }
    
    func appendData() {
        execises.append(ExeciseObject(title: "Balance", subTitle: "4-4-4 breathing", breathingType: ExeciseType.Balance.rawValue, image: "Balance",duration: 1,inhaleSecond: 4, holdSecond: 4, exhaleSecond: 4, mainDescription: "The 4-4-4 breathing technique is based on the principles of Sama Vritti, a yoga practice known as \"equal breathing\" or “Box breathing”. By balancing the inhalation and exhalation durations, it aims to connect the mind with the body, a fundamental concept in many Eastern wellness practices.", execiseDescription: "Close your lips and inhale quietly through your nose for a count of four. Hold your breath for a count of four. Exhale completely through your mouth for a count of four, maintaining a gentle and steady breath out. This completes one cycle of balanced breathing.", scienceDescription: "This technique helps balance your body's stress response system. When you breathe in, hold, and breathe out at the same time, your body gets a signal to calm down. It's like telling your body everything is balanced and okay, so you feel more relaxed and steady."))
        execises.append(ExeciseObject(title: "Focus", subTitle: " 4-4-8 breathing", breathingType: ExeciseType.Focus.rawValue, image: "Focus",duration: 1,inhaleSecond: 4,holdSecond: 4 , exhaleSecond: 8, mainDescription: "The 4-4-8 breathing technique is similar to the pranayama practices in yoga, this technique emphasizes prolonged exhalation to induce a state of mental clarity and focus. It is a technique like Nadi Shodhana (alternate nostril breathing) which is used to clear the mind and optimize concentration.", execiseDescription: "Close your lips and inhale slowly through your nose for a count of four. Hold your breath for a count of four. Exhale completely through your mouth for a count of eight, making a gentle whoosh sound as you breathe out. This completes one cycle of Focus breathing.", scienceDescription: "By breathing out longer than you breathe in, your body slows down, especially your heart rate. This helps clear your mind. It's like pressing a 'slow down' button inside you, making you feel more focused and less scattered."))
        execises.append(ExeciseObject(title: "Energise", subTitle: "2-2-4 breathing", breathingType: ExeciseType.Energise.rawValue, image: "Energize",duration: 1 , inhaleSecond: 2,holdSecond: 2 , exhaleSecond: 4, mainDescription: "The 2-2-4 breathing technique is inspired by the Bhastrika or Kapalabhati pranayama in yoga, which is known to invigorate the body and refresh the mind. These practices involve rapid, energizing breaths to stimulate the body's energy channels.", execiseDescription: "Close your lips and take a quick inhale through your nose for a count of two. Hold your breath for a count of two. Exhale completely through your mouth for a count of four, with a brisk and energetic whoosh. This completes one cycle of energise breathing.", scienceDescription: "Quick breathing increases your heartbeat a little, making you feel more awake and alert. It's like a quick energy boost for your brain and body. Imagine giving your body a mini adrenaline rush without the stress."))
        execises.append(ExeciseObject(title: "Relax", subTitle: "4-7-8 breathing", breathingType: ExeciseType.Relax.rawValue ,image: "Relax",duration: 1,inhaleSecond: 4, holdSecond: 7, exhaleSecond: 8, mainDescription: "The 4-7-8 breathing technique is popularized by Dr. Andrew Weil, drawing from the pranayama tradition. It’s specifically designed to calm the nervous system and promote relaxation, similar to the calming effects of practices like Anulom Vilom.", execiseDescription: "Close your lips and inhale quietly through your nose for a count of four. Hold your breath for a count of seven. Exhale completely through your mouth for a count of eight, making a whoosh sound. This completes one cycle of Relax breathing.", scienceDescription: "Longer exhales help your body to relax deeply. It's like a natural 'chill out' signal to your brain. When you breathe out for a long time, your body gets the message to slow down and relax, helping you to feel calmer."))
        execises.append(ExeciseObject(title: "Strengthen", subTitle: "1-0-1 breathing", breathingType: ExeciseType.Strengthen.rawValue,image: "Strengthen",duration: 1,inhaleSecond: 1,holdSecond: 0 , exhaleSecond: 1, mainDescription: "The 1-0-1 breathing technique echoes the principles of strengthening and conditioning found in various physical disciplines. It's similar in spirit to yogic exercises that focus on enhancing lung capacity and respiratory strength, though it's more aligned with athletic training practices.", execiseDescription: "Close your lips and inhale sharply through your nose for a count of one. Immediately exhale through your mouth for a count of one, with a quick and forceful whoosh. This completes one cycle of Strengthen breathing.", scienceDescription: "Fast breathing is like a workout for your lungs. It makes your breathing muscles stronger and more efficient, just like how lifting weights strengthens your arms. Over time, this can make your lungs better at getting air in and out."))
        execises.append(ExeciseObject(title: "Calm", subTitle: "4-2-6 breathing", breathingType: ExeciseType.Calm.rawValue, image: "Calm",duration: 1,inhaleSecond: 4,holdSecond: 2 , exhaleSecond: 6, mainDescription: "The 4-2-6 breathing technique is similar to many calming exercises found in yoga. It's like the yoga breathing called Ujjayi, which is known for helping you feel relaxed and at ease.", execiseDescription: "Close your lips and inhale gently through your nose for a count of four. Hold your breath for a count of two. Exhale completely through your mouth for a count of six, making a smooth and calming whoosh sound. This completes one cycle of Calm breathing.", scienceDescription: "This pattern helps reduce anxiety by making your body focus on the rhythm of your breath. The longer exhale helps to slow down your heart rate, making you feel more peaceful. It's like using your breath to tell your body to take it easy and relax."))
        execises.append(ExeciseObject(title: "Paced Breathing", subTitle: "4-3-6 breathing", breathingType: ExeciseType.PacedBreathing.rawValue, image: "PacedBreathing",duration: 1,inhaleSecond: 4,holdSecond: 3 , exhaleSecond: 6, mainDescription: "The 4-3-6 breathing technique is a mindful breathing practice designed to promote relaxation and stress reduction. It involves a specific rhythm of breathing where the inhalation, breath holding, and exhalation are paced in a 4-3-6 sequence. This method is not only easy to remember but also effective in managing stress and anxiety.", execiseDescription: "Close your lips and inhale through your nose for a count of four. Hold your breath for a count of three, allowing the oxygen to circulate throughout your body. Finally, complete the cycle by exhaling slowly and fully through your mouth for a count of six, releasing any tension or stress.", scienceDescription: "The 4-3-6 breathing method works by regulating the autonomic nervous system, which controls the body's stress response. The extended exhalation (six seconds) in comparison to the inhalation (four seconds) activates the parasympathetic nervous system, often referred to as the \"rest and digest\" system. This helps in reducing heart rate and blood pressure, leading to a state of calm and relaxation."))
        ExeciseDao.saveOrUpdateUser(execise: execises)
        execises = ExeciseDao.getAllExecise().filter({ $0.breathingType == "Balance" || $0.breathingType == "Relax"})
    }
    
    func setUpRotateImage() {
        animationIV.image = UIImage(named: "homeFlower")
        let rotationAngle: CGFloat = .pi
        UIView.animate(withDuration: 1.5, delay: 0.0, options: .repeat, animations: {
            self.animationIV.transform = self.animationIV.transform.rotated(by: rotationAngle)
        }, completion: nil)
    }
    
    @IBAction func actionOnSos(){
        let vc = SOSVC.instantiateFromStoryboard()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnPremium(){
        let vc = PayWallVC.instantiateFromStoryboard()
        vc.isFromHome = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnPacedBreath(){
        let vc = RelaxVC.instantiateFromStoryboard()
        vc.execise = ExeciseDao.getSingleExecise(breathType: ExeciseType.PacedBreathing.rawValue)
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.isEmpty ? execises.count : data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTVC", for: indexPath) as! HomeTVC
        if data.isEmpty {
            cell.configCell(execise: execises[indexPath.row])
        } else {
            cell.configCell(habit: data[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = RelaxVC.instantiateFromStoryboard()
        vc.isFromTask = true
        vc.execise = data.isEmpty ? execises[indexPath.row] : data[indexPath.row].execise
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
}
