//
//  HomeTVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 16/11/23.
//

import UIKit

class HomeTVC: UITableViewCell {
    
    @IBOutlet var titleL: UILabel!
    @IBOutlet var timeL: UILabel!
    @IBOutlet var profileIV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(habit: DailyHabit) {
        titleL.text = habit.execise?.title
        timeL.text = habit.time.hasPrefix("Task") ? habit.time : habit.time + " | " + String(habit.duration) + "min"
        profileIV.image = UIImage(named: habit.execise?.image ?? "")
    }
    
    func configCell(execise: ExeciseObject) {
        titleL.text = execise.title
        timeL.text = "Today task"
        profileIV.image = UIImage(named: execise.image)
    }
}
