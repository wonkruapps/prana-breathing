//
//  PayWallVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 20/11/23.
//

import Foundation
import UIKit

class PayWallVC: BaseVC {
    
    static let name = "PayWallVC"
    static let storyBoard = "PayWall"
    
    class func instantiateFromStoryboard() -> PayWallVC {
        let vc = UIStoryboard(name: PayWallVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: PayWallVC.name) as! PayWallVC
        return vc
    }
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var purchaseV: [UIView]!
    @IBOutlet var purchaseIV: [UIImageView]!
    @IBOutlet var saveV: UIView!
    @IBOutlet var freeTrailL: UILabel!
    @IBOutlet var navigationBarV: NavigationBar!
    @IBOutlet var freeTrailSwitch: UISwitch!
    
    var isFromHome = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView() {
        navigationBarV.infoIV.isHidden = true
        navigationBarV.backIV.image = UIImage(systemName: "xmark")
        navigationBarV.rightCornerL.text = "Restore"
        navigationBarV.popViewController = {
            if self.isFromHome {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.dismiss(animated: true)
                Utils.setRootVC(.homeVC)
            }
        }
        navigationBarV.restorePayWall = {
            self.actionOnRestore()
        }
        saveV.setCornerRadius(radius: 8)
        cornerV.forEach({ $0.setCornerRadius(radius: 16 , borderColor: Utils.theme == 0 ? UIColor.white.withAlphaComponent(0.7) : UIColor.white.withAlphaComponent(0.15)) })
        enableFreeTrailOnSelection(sender: 0)
    }
    
    @IBAction func actionOnFreeTrail(sender: UISwitch) {
        if sender.isOn {
            enableFreeTrailOnSelection(sender: 1)
        } else {
            enableFreeTrailOnSelection(sender: 0)
        }
        
    }
    
    @IBAction func actionOnSelection(sender: UIButton) {
        enableFreeTrailOnSelection(sender: sender.tag)
    }
    
    @IBAction func actionOnPrivacy() {
        openSafariWebView(urlString: PRIVACY_POLICY)
    }
    
    @IBAction func actionOnTermsAndCondition() {
        openSafariWebView(urlString: TERMS_AND_CONDITION)
    }
    
    @IBAction func actionOnContinue() {
        if freeTrailSwitch.isOn {
            Utils.showHUD(view: view)
            IAPManager.shared.purchase(product: .weekly)
        } else {
            Utils.showHUD(view: view)
            IAPManager.shared.purchase(product: .yearly)
        }
    }
    
    override func inAppPurchasePurchased(notification: Notification) {
        Utils.hideHUD(view: view)
        Utils.setRootVC(.homeVC)
    }
    
    override func inAppPurchaseFailed(notification: Notification) {
        super.inAppPurchaseFailed(notification: notification)
        Utils.hideHUD(view: view)
    }
    
    func enableFreeTrailOnSelection(sender: Int) {
        for (index, view) in purchaseV.enumerated() {
            if sender == index {
                freeTrailSwitch.isOn = true
                freeTrailL.text = "Free Trial Enabled"
                view.setBorderColor(borderColor: UIColor(hexStr: "6F5DDF"))
                purchaseIV[index].image = UIImage.paywallFillCheckBox
            } else {
                freeTrailSwitch.isOn = false
                freeTrailL.text = "Enable Free Trial"
                view.setBorderColor(borderColor: Utils.theme == 0 ? UIColor.white.withAlphaComponent(0.7) : UIColor.white.withAlphaComponent(0.15))
                purchaseIV[index].image = UIImage.paywallEmptyCheckBox
            }
        }
    }
    
    func actionOnRestore() {
        enableFreeTrailOnSelection(sender: 0)
    }
}
