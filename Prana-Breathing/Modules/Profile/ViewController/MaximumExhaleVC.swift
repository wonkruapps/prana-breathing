//
//  MaximumExhaleVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 06/11/23.
//

import Foundation
import UIKit
import Lottie

class MaximumExhaleVC: BaseVC {
    static let name = "MaximumExhaleVC"
    static let storyBoard = "MaximumExhale"
    
    class func instantiateFromStoryboard() -> MaximumExhaleVC {
        let vc = UIStoryboard(name: MaximumExhaleVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: MaximumExhaleVC.name) as! MaximumExhaleVC
        return vc
    }
    
    @IBOutlet var secondsL: UILabel!
    @IBOutlet var animationBorderV: UIView!
    @IBOutlet var animationV: UIView!
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var imageViews: [UIImageView]!
    @IBOutlet var premiumStarV: UIView!
    @IBOutlet var averageV: UIView!
    @IBOutlet var navigationBarV: NavigationBar!
    @IBOutlet var eliteV: UIView!
    @IBOutlet var optimalV: UIView!
    @IBOutlet var averageLs: [UILabel]!
    @IBOutlet var eliteLs: [UILabel]!
    @IBOutlet var optimalLs: [UILabel]!
    @IBOutlet var startBtnL: UILabel!
    
    var screenType: ProfileType = .breathHold
    var timer: Timer?
    var dummySeconds = 0
    var dummyMinutes = 0
    var secondsCount = 0
    var isTimerStarted = false
    
    let animationView = LottieAnimationView(animation: LottieAnimation.named(Utils.theme == 0 ? "BreathingAnimationLight" : "BreathingAnimationDark"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        setUpView()
        setUpImage(currentIndex: -1)
        circleAnimation()
    }

    ///             IBACTIONS
    
    @IBAction func actionOnStartTest(_ sender: Any) {
        isTimerStarted.toggle()
        if isTimerStarted {
            animationView.play()
            startBtnL.text = "Reset test"
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(UpdateSeconds), userInfo: nil, repeats: true)
        } else {
            timer?.invalidate()
            startBtnL.text = "Start new test"
            resetView()
            animationView.stop()
            secondsL.text = "00:00"
            if screenType == .breathHold {
                CustomUserDefaults.maximumBreathHold = CustomUserDefaults.maximumBreathHold >= secondsCount ? CustomUserDefaults.maximumBreathHold : secondsCount
            } else {
                CustomUserDefaults.maximumExhale = CustomUserDefaults.maximumExhale >= secondsCount ? CustomUserDefaults.maximumExhale : secondsCount
            }
            secondsCount = 0
            dummyMinutes = 0
            dummySeconds = 0
        }
        
    }
    
    @objc func UpdateSeconds() {
        updateLabel()
        changeView()
    }
    
    func updateLabel() {
        dummySeconds += 1
        secondsCount += 1
        if dummySeconds == 60 {
            dummySeconds = 0
            dummyMinutes += 1
        }
        let formattedTime = String(format: "%02d:%02d", dummyMinutes, dummySeconds)
        secondsL.text = formattedTime
    }
    
    func resetView() {
        setUpBackGround(currentIndex: -1)
        setUpImage(currentIndex: -1)
        averageLs.forEach({ $0.textColor = UIColor.primaryColor })
        optimalLs.forEach({ $0.textColor = UIColor.primaryColor })
        eliteLs.forEach({ $0.textColor = UIColor.primaryColor })
    }
    
    func changeView() {
        if secondsCount > 0 && secondsCount < 50 {
            setUpBackGround(currentIndex: 0)
            setUpImage(currentIndex: 0)
            averageLs.forEach({ $0.textColor = Utils.theme == 1 ? .black : .white})
            optimalLs.forEach({ $0.textColor = UIColor(hexStr: "8F8FE2") })
            eliteLs.forEach({ $0.textColor = UIColor(hexStr: "8F8FE2") })
        } else if secondsCount > 50 && secondsCount < 80 {
            setUpBackGround(currentIndex: 1)
            setUpImage(currentIndex: 1)
            averageLs.forEach({ $0.textColor = UIColor(hexStr: "8F8FE2") })
            optimalLs.forEach({ $0.textColor = Utils.theme == 1 ? .black : .white })
            eliteLs.forEach({ $0.textColor = UIColor(hexStr: "8F8FE2") })
        } else if secondsCount > 80 {
            setUpBackGround(currentIndex: 2)
            setUpImage(currentIndex: 2)
            averageLs.forEach({ $0.textColor = UIColor(hexStr: "8F8FE2") })
            optimalLs.forEach({ $0.textColor = UIColor(hexStr: "8F8FE2") })
            eliteLs.forEach({ $0.textColor = Utils.theme == 1 ? .black : .white })
        } else if secondsCount == 120 {
            resetView()
            timer?.invalidate()
        }
    }
    
    func setUpNavigationBar() {
        navigationBarV.titleL.text = screenType == .maximumExhale ? "Maximum exhale" : "Breath hold"
        navigationBarV.titleL.textColor = Utils.theme == 0 ? .black : .white
        navigationBarV.infoIV.isHidden = true
        navigationBarV.backIV.tintColor = Utils.theme == 0 ? .black : .white
        navigationBarV.popViewController = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpView() {
        premiumStarV.setCornerRadius(radius: 25)
        cornerV.forEach({ $0.setCornerRadius(radius: 16)})
        animationBorderV.setCornerRadius(radius: 85, borderColor: UIColor.primaryColor ?? UIColor())
    }
    
    func setUpImage(currentIndex: Int) {
        imageViews.enumerated().forEach { index, imageView in
            if index == currentIndex {
                imageView.image = UIImage(named: screenType == .maximumExhale ? "LungsWhite" : "mouthWhite")
            } else {
                imageView.image = UIImage(named: screenType == .maximumExhale ? "Lungs" : "mouth")
            }
        }
    }
    
    func setUpBackGround(currentIndex: Int) {
        cornerV.enumerated().forEach({ index, view in
            if index == currentIndex {
                view.backgroundColor = Utils.theme == 1 ? UIColor(hexStr: "6F5DDF") : UIColor.primaryColor
            } else {
                view.backgroundColor = Utils.theme == 1 ? UIColor(hexStr: "6F5DDF").withAlphaComponent(0.3) : UIColor(hexStr: "EAEAFC")
            }
        })
    }
    
    
    // Create Animation object
      func circleAnimation() {
        animationView.loopMode = .autoReverse
        animationView.animationSpeed = 0.5
        animationView.contentMode = .scaleToFill
        animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        animationV.addSubview(animationView)
      }
}
