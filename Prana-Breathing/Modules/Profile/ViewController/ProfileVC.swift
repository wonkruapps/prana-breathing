//
//  Profile.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 03/11/23.
//

import Foundation
import UIKit
import Charts
import JKCircularProgess

enum Profile {
    case stats
    case progress
}

class ProfileVC: BaseVC {
    
    static let name = "ProfileVC"
    static let storyBoard = "ProfileScreen"
    
    class func instantiateFromStoryboard() -> ProfileVC {
        let vc = UIStoryboard(name: ProfileVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: ProfileVC.name) as! ProfileVC
        return vc
    }
    
    // IBOutlet Declaration
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var clearBorderV : [UIView]!
    @IBOutlet var profileL : UILabel!
    @IBOutlet var nameL : UILabel!
    @IBOutlet var memberL : UILabel!
    @IBOutlet var dayLs : [UILabel]!
    @IBOutlet var emojiDayLs : [UILabel]!
    @IBOutlet var steakL : UILabel!
    @IBOutlet var sessionL : UILabel!
    @IBOutlet var minutesL : UILabel!
    @IBOutlet var breathHoldSecL : UILabel!
    @IBOutlet var exhaleSecL : UILabel!
    @IBOutlet var profileHiddenV: [UIView]!
    @IBOutlet var progressTickIV: [UIImageView]!
    @IBOutlet var emojiIV: [UIImageView]!
    @IBOutlet var statsL: UILabel!
    @IBOutlet var progressL: UILabel!
    @IBOutlet var progressHiddenV: [UIView]!    
    @IBOutlet var barChartView: BarChartView!
    @IBOutlet var customJKView: [CircularProgressBar]!
    
    var chartData = [ChartDataModel]()
    var angles = [Double]()
    
    var profile: Profile = .stats
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
        setUpData()
        setUpProgress()
        setupBarChart()
    }
    
    // IBActions
    
    
    @IBAction func actionOnBreathHold(_ sender: Any) {
        let vc = MaximumExhaleVC.instantiateFromStoryboard()
        vc.hidesBottomBarWhenPushed = true
        vc.screenType = .breathHold
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func actionOnMaximumExhale(_ sender: Any) {
        let vc = MaximumExhaleVC.instantiateFromStoryboard()
        vc.hidesBottomBarWhenPushed = true
        vc.screenType = .maximumExhale
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnStats(_ sender: Any) {
        profile = .stats
        profileHiddenV.forEach({ $0.isHidden = false })
        progressHiddenV.forEach({ $0.isHidden = true })
        let defaultTheme = self.traitCollection.userInterfaceStyle
        if defaultTheme == .light {
            progressL.backgroundColor = .clear
            statsL.backgroundColor =  UIColor.white
        } else if defaultTheme == .dark {
            progressL.backgroundColor = .clear
            statsL.backgroundColor =  UIColor(hexStr: "000000")
        }
        statsL.textColor =  UIColor.primaryTextColor
        progressL.textColor = UIColor.SecondaryTextColor
    }
    
    @IBAction func actionOnProgress(_ sender: Any) {
            profile = .progress
            profileHiddenV.forEach({ $0.isHidden = true })
            progressHiddenV.forEach({ $0.isHidden = false })
            if Utils.theme == 0 {
                statsL.backgroundColor = .clear
                progressL.backgroundColor =  UIColor.white
            } else {
                statsL.backgroundColor = .clear
                progressL.backgroundColor =  UIColor(hexStr: "000000")
            }
            progressL.textColor = UIColor.primaryTextColor
            statsL.textColor = UIColor.SecondaryTextColor
    }
    
    // Supporting Functions
    
    func setUpData() {
        var totalValue = 0
        let totalTaskCount = DailyHabitDao.getHabitsByDay(day: Date().getFormattedDay()).count == 0 ? 2 : DailyHabitDao.getHabitsByDay(day: Date().getFormattedDay()).count
        let data = ChartDataDao.getAllChartData()
        ChartDataDao.getAllChartData().forEach({ totalValue += $0.value })
        angles = Array(repeating: 0.0, count: 7)
        for element in data {
            let currentDayIndex = Utils.days.firstIndex(of: element.day)
            let angleValue = Double(totalValue) / Double(totalTaskCount)
            angles[currentDayIndex ?? 0] = angleValue
            emojiIV[currentDayIndex ?? 0].image = UIImage(named: element.emotion)
        }
    }
    
    func setUpView() {
        navigationController?.navigationBar.isHidden = true
        nameL.text = CustomUserDefaults.userName.capitalized
        minutesL.text = "\(CustomUserDefaults.getTotalMinute())"
        steakL.text = "\(CustomUserDefaults.getCurrentStreak())"
        sessionL.text = "\(CustomUserDefaults.getTotalSession())"
        breathHoldSecL.text = "\(CustomUserDefaults.maximumBreathHold)s"
        exhaleSecL.text = "\(CustomUserDefaults.maximumExhale)s"
        profileL.text = CustomUserDefaults.userName.first?.uppercased()
        profileL.backgroundColor = UIColor.randomColor
        profileL.setCornerRadius(radius: 35)
        emojiIV.forEach({ $0.image = UIImage(named: Emoji.Good.rawValue) })
        if profile == .stats {
            if Utils.theme == 0 {
                cornerV.forEach({ $0.setCornerRadius(radius: 16, borderColor: UIColor(hexStr: "000000").withAlphaComponent(0.3)) })
                statsL.backgroundColor = UIColor.white
            } else {
                cornerV.forEach({ $0.setCornerRadius(radius: 16, borderColor: UIColor(hexStr: "FFFFFF").withAlphaComponent(0.3)) })
                statsL.backgroundColor = UIColor(hexStr: "000000")
            }
            progressHiddenV.forEach({ $0.isHidden = true })
        } else {
            if Utils.theme == 0 {
                cornerV.forEach({ $0.setCornerRadius(radius: 16, borderColor: UIColor(hexStr: "000000").withAlphaComponent(0.3)) })
                progressL.backgroundColor = UIColor.white
            } else {
                cornerV.forEach({ $0.setCornerRadius(radius: 16, borderColor: UIColor(hexStr: "FFFFFF").withAlphaComponent(0.3)) })
                progressL.backgroundColor = UIColor(hexStr: "000000")
            }
            profileHiddenV.forEach({ $0.isHidden = true })
        }
        for (index, label) in dayLs.enumerated() {
            let formattedDay = Date().getFormattedDay()
            if formattedDay == Utils.days[index] {
                label.textColor = Utils.theme == 0 ? .black : .white
                label.text = String(formattedDay.prefix(1))
                emojiDayLs[index].text = String(formattedDay.prefix(1))
                emojiDayLs[index].textColor = Utils.theme == 0 ? .black : .white
            } else {
                label.text = String(Utils.days[index].prefix(1))
                emojiDayLs[index].text = String(Utils.days[index].prefix(1))
            }
            
        }
        clearBorderV.forEach({ $0.setCornerRadius(radius: 16) })
        progressL.setCornerRadius(radius: 10)
        statsL.setCornerRadius(radius: 10)
    }
    
    func setUpProgress() {
        progressTickIV.forEach({ $0.image = UIImage(named: "ProgressTick") })
        for (index, progress) in customJKView.enumerated() {
            if angles[index] == 1.0 {
                progressTickIV[index].isHidden = false
                progress.setProgress(progress: angles[index])
            } else {
                progressTickIV[index].isHidden = true
                progress.setProgress(progress: angles[index])
            }
        }
    }
    
    func setupBarChart() {
        var colorList = [UIColor]()
        let datas = [1,2,3,4,5,6,7]
        var dataEntries: [BarChartDataEntry] = []
        let totalTaskCount = DailyHabitDao.getHabitsByDay(day: Date().getFormattedDay()).count == 0 ? 2 : DailyHabitDao.getHabitsByDay(day: Date().getFormattedDay()).count
        for (index,data) in datas.enumerated() {
            dataEntries.append(BarChartDataEntry(x: Double(data), y: Double(angles[index] * Double(totalTaskCount))))
        }
        let chartDataSet = BarChartDataSet(entries: dataEntries)
        let chartData = BarChartData(dataSet: chartDataSet)
        
        barChartView.data = chartData
        colorList.append(UIColor.primaryColor ?? .clear)
        
        // Customize bar colors
        chartDataSet.colors = colorList

        // Customize value label format
        let valueFormatter = DefaultValueFormatter(decimals: 0)
        chartData.setValueFormatter(valueFormatter)
        chartDataSet.drawValuesEnabled = false
        
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = false
        
        barChartView.maxVisibleCount = 60
        
        let xAxis = barChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = UIFont(name: "Inter-Medium", size: 16) ?? UIFont()
        xAxis.granularityEnabled = true
        xAxis.valueFormatter = DayAxisValueFormatter()
        xAxis.labelTextColor = Utils.theme == 0 ? UIColor(hexStr: "000000").withAlphaComponent(0.5) : UIColor.white.withAlphaComponent(0.5)
        xAxis.drawGridLinesEnabled = false
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.positiveSuffix = "m"
        
        let leftAxis = barChartView.leftAxis
        leftAxis.labelCount = 0
        leftAxis.enabled = false
        
        let rightAxis = barChartView.rightAxis
        rightAxis.enabled = true
        rightAxis.labelFont = UIFont(name: "Inter-Medium", size: 16) ?? UIFont()
        rightAxis.labelCount = 5
        rightAxis.labelTextColor = Utils.theme == 0 ? UIColor(hexStr: "000000").withAlphaComponent(0.5) : UIColor.white.withAlphaComponent(0.5)
        rightAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        rightAxis.axisMinimum = 0
        rightAxis.axisMaximum = 5
        
        barChartView.legend.form = .empty
        barChartView.legend.textColor = .clear
    }
    
}

class DayAxisValueFormatter: NSObject, AxisValueFormatter {
    let daysOfWeek = ["", "M", "T", "W", "T", "F", "S", "S"]

    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value)
        if index >= 0, index < daysOfWeek.count {
            return daysOfWeek[index]
        }
        return ""
    }
}
