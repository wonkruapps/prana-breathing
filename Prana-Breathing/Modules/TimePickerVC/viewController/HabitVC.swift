//
//  HabitVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 09/11/23.
//

import Foundation
import UIKit
import RealTimePicker

class HabitVC: BaseVC {
    static let name = "HabitVC"
    static let storyBoard = "TimePickerScreen"
    
    @IBOutlet var cornerV: [UIView]!
    @IBOutlet var dayL: [UILabel]!
    @IBOutlet var timePickerV: UIView!
    @IBOutlet var buttonV: UIView!
    @IBOutlet var navigationBarV: NavigationBar!
    
    var set = Set<Int>()
    var hour = ""
    var minute = ""
    var noonFormat = ""
    var execise: ExeciseObject?
    let timePicker = RealTimePickerView(format: .h12 , tintColor: .white)
    
    class func instantiateFromStoryboard() -> HabitVC {
        let vc = UIStoryboard(name: HabitVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: HabitVC.name) as! HabitVC
        return vc
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
    }
    
    func setUpView() {
        setUpNavigationBar()
        cornerV.forEach({ $0.setCornerRadius(radius: 21) })
        buttonV.setCornerRadius(radius: 16)
        setUpTimePicker()
        let habits = DailyHabitDao.getAllHabits()
        for (index, day) in Utils.days.enumerated() {
            if !habits.filter({ $0.breathingType == execise?.breathingType && $0.day == day }).isEmpty {
                updateBackGround(index: index)
            }
        }
        print(habits)
    }
    
    func setUpNavigationBar() {
        navigationBarV.infoIV.isHidden = true
        navigationBarV.titleL.text = "Make this a habit"
        navigationBarV.popViewController = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpTimePicker() {
        timePicker.showUnitSeparator = true
        timePicker.rowHeight = 40.0
        timePicker.timeLabelFont = UIFont(name: "Inter-Bold", size: 24)
        timePicker.colonLabelFont = UIFont(name: "Inter-Bold", size: 24)
        timePicker.formatLabelFont = UIFont(name: "Inter-Bold", size: 24)
        timePicker.backgroundColor = .clear
        timePicker.showCurrentTime = true
        timePicker.frame = CGRect(x: 0, y: 0, width: 250, height: 120)
        timePicker.onNumberTimePicked = { hour, minute in
            if hour >= 0 && hour < 12 {
                self.hour = String(hour)
                self.noonFormat = "AM"
            } else if hour >= 12 && hour < 24 {
                self.hour = String(hour - 12)
                self.noonFormat = "PM"
            }
            self.minute = String(minute)
        }
        timePickerV.addSubview(timePicker)
    }
    
    @IBAction func actionOnSelection(sender: UIButton) {
        updateBackGround(index: sender.tag)
    }
    
    func updateBackGround(index: Int) {
        if set.contains(index) {
            cornerV[index].backgroundColor = .white
            dayL[index].textColor = UIColor.primaryColor
            let data = DailyHabitDao.getAllHabits().first(where: { $0.breathingType == execise?.breathingType && $0.day == Utils.days[index]} )
            if data != nil {
                DailyHabitDao.deleteHabit(habit: data ?? DailyHabit())
            }
            set.remove(index)
        } else {
            cornerV[index].backgroundColor = UIColor.darkModePrimary
            dayL[index].textColor = .white
            set.insert(index)
        }
    }
    
    @IBAction func actionOnSave() {
        if hour.isEmpty || minute.isEmpty || noonFormat.isEmpty {
            splitHourMinuteNoon()
        }
        set.forEach({
            let habit = DailyHabit()
            habit.tempId = UUID().uuidString
            habit.day = Utils.days[$0]
            habit.hour = hour == "0" ? "12" : hour
            habit.minute = Int(minute) ?? 0 < 10 ? "0\(minute)" : minute
            habit.time = "\(hour == "0" ? "12" : hour):\(minute) \(noonFormat)"
            habit.noon = noonFormat
            habit.duration = execise?.duration ?? 1
            habit.execise = execise
            habit.breathingType = execise!.breathingType
            DailyHabitDao.saveOrUpdateUser(habit: habit)
        })
        if CustomUserDefaults.isNotificationOn {
            let habitTime = DailyHabitDao.getAllHabits().filter({ $0.day == Date().getFormattedDay() })
            let times = habitTime.compactMap({ $0.time })
            CustomUserDefaults.setNotificatonScheduleTime(data: times)
            scheduleNotification()
            navigationController?.popViewController(animated: true)
        } else {
            let vc = NotificationVC.instantiateFromStoryboard()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func splitHourMinuteNoon() {
        let currentDate = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour, .minute, .timeZone], from: currentDate)

        guard let hour = components.hour, let minute = components.minute else { return }
        
        self.hour = hour >= 12 ? String(hour - 12) : hour == 0 ? "12" : String(hour)
        self.minute = String(minute)
        noonFormat = components.hour! >= 12 ? "PM" : "AM"
        
    }
}
