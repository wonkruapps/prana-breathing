//
//  InfoVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 07/11/23.
//

import Foundation
import UIKit

class InfoVC: BaseVC {
    
    static let name = "InfoVC"
    static let storyBoard = "InfoScreen"
    
    class func instantiateFromStoryboard() -> InfoVC {
        let vc = UIStoryboard(name: InfoVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: InfoVC.name) as! InfoVC
        return vc
    }
    
    @IBOutlet var navigationBarV: NavigationBar!
    @IBOutlet var subTitleL: UILabel!
    @IBOutlet var mainDescriptionL: UILabel!
    @IBOutlet var execiseDescriptionL: UILabel!
    @IBOutlet var scienceDescriptionL: UILabel!
    @IBOutlet var inhaleSecondL: UILabel!
    @IBOutlet var exhaleSecondL: UILabel!
    
    var execise: ExeciseObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigationBar()
        setUpView()
    }
    
    func setUpNavigationBar() {
        navigationBarV.infoIV.isHidden = true
        navigationBarV.titleL.text = execise?.title
        navigationBarV.popViewController = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpView() {
        subTitleL.text = execise?.subTitle
        mainDescriptionL.text = execise?.mainDescription
        execiseDescriptionL.text = execise?.execiseDescription
        scienceDescriptionL.text = execise?.scienceDescription
        inhaleSecondL.text = "\(execise?.inhaleSecond ?? 0)"
        exhaleSecondL.text = "\(execise?.exhaleSecond ?? 0)"
    }
    
}
