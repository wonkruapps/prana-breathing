//
//  PranaTabBarController.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 08/11/23.
//

import Foundation
import UIKit

class PranaTabBarController: UITabBarController {
    static let name = "PranaTabBarController"
    static let storyBoard = "Main"
    
    /// The caller of this class does not need to know how we instantiate it.
    /// We simply return the instantiated class to the caller and they invoke it how they want
    /// If the as! fails, it will fail upon immediate testing
    class func instantiateFromStoryboard() -> PranaTabBarController {
        let vc = UIStoryboard(name: PranaTabBarController.storyBoard, bundle: nil).instantiateViewController(withIdentifier: PranaTabBarController.name) as! PranaTabBarController
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func createTabbarControllers() {
        let homeViewController = HomeVC.instantiateFromStoryboard()
        let homeVC = CustomNavigationController(rootViewController: homeViewController)
        homeViewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "homeIcon"), tag: 1)
        
        let BreathTechniquesVC = BreathTechniquesVC.instantiateFromStoryboard()
        let breathVC = CustomNavigationController(rootViewController: BreathTechniquesVC)
        BreathTechniquesVC.tabBarItem = UITabBarItem(title: "Breathe", image: UIImage(systemName: "record.circle.fill") , tag: 2)
        
        let profileViewController = ProfileVC.instantiateFromStoryboard()
        let ProfileVC = CustomNavigationController(rootViewController: profileViewController)
        profileViewController.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(systemName: "person.crop.circle.fill") , tag: 3)
        
        viewControllers = [homeVC, breathVC, ProfileVC]
        selectedIndex = 0
    }
    
    func setupViews() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        tabBar.isTranslucent = false
        tabBar.unselectedItemTintColor = .gray
        tabBar.tintColor = Utils.theme == 0 ? .black : .white
        createTabbarControllers()
        tabBar.clipsToBounds = true
        tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        tabBar.layer.shadowOpacity = 0.5
        tabBar.layer.shadowOffset = CGSize.zero
        tabBar.layer.shadowRadius = 2
        tabBar.layer.borderColor = UIColor(red: 0.388, green: 0.451, blue: 0.506, alpha: 0.3).cgColor
        tabBar.layer.borderWidth = 0
        tabBar.clipsToBounds = false
    }
}
