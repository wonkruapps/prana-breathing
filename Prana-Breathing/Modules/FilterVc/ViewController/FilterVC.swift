//
//  FilterVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 08/11/23.
//

import Foundation
import UIKit

class FilterVC: BaseVC {
    
    static let name = "FilterVC"
    static let storyBoard = "FilterScreen"
    
    class func instantiateFromStoryboard() -> FilterVC {
        let vc = UIStoryboard(name: FilterVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: FilterVC.name) as! FilterVC
        return vc
    }
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var navigationBarV: NavigationBar!
    @IBOutlet var durationIV: UIImageView!
    @IBOutlet var buttonV: UIView!
    @IBOutlet var musicL: UILabel!
    @IBOutlet var themeSwitch: SwitchBtn!
    @IBOutlet var backgroundMusicSwitch: SwitchBtn!
    @IBOutlet var pageController: UIPageControl!
    @IBOutlet var vibrationToggle: SwitchBtn!
    
    var execise: ExeciseObject?
    var currentPage = 0
    var duration = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigationBar()
        loadImage()
        setUpView()
    }
    
    func setUpView() {
        musicL.text = CustomUserDefaults.getBackgroundMusic()
        backgroundMusicSwitch.isOn = CustomUserDefaults.isBackgroundMusicOn
        vibrationToggle.isOn = CustomUserDefaults.isVibrationOn
        themeSwitch.isOn = Utils.theme == 0
        buttonV.setCornerRadius(radius: 16)
    }
    
    func loadImage() {
        switch execise?.duration {
        case 1:
            durationIV.image = UIImage(named: "OneMinuteImage")
        case 3:
            durationIV.image = UIImage(named: "ThreeminutesImage")
        case 5:
            durationIV.image = UIImage(named: "fiveMinutesImage")
        default:
            break
        }
    }
    
    @IBAction func actionOnOneMinute(_ sender: Any) {
        durationIV.image = UIImage(named: "OneMinuteImage")
        duration = 1
    }
    
    @IBAction func actionOnThemeToggle(sender: UISwitch) {
        if sender.isOn {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .light
                Utils.theme = 0
            }
            collectionView.reloadData()
        } else {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .dark
                Utils.theme = 1
            }
            collectionView.reloadData()
        }
    }
    
    @IBAction func actionOnBackgroundMusicToggle(sender: UISwitch) {
        if sender.isOn {
            guard let musicFile = CustomUserDefaults.getBackgroundMusic(), musicFile != "" else {
                Utils.showAlert(message: "Please select any audio", viewController: self)
                sender.isOn = false
                return
            }
            CustomUserDefaults.isBackgroundMusicOn = true
        } else {
            CustomUserDefaults.isBackgroundMusicOn = false
        }
    }
    
    @IBAction func actionOnVibration(sender: UISwitch) {
        if sender.isOn {
            CustomUserDefaults.isVibrationOn = true
        } else {
            CustomUserDefaults.isVibrationOn = false
        }
    }
    
    @IBAction func actionOnBackgroundSound() {
        let vc = BackgroundSoundVC.instantiateFromStoryboard()
        present(vc, animated: true)
    }
    
    @IBAction func actionOnSave(_ sender: Any) {
        let type = currentPage == 0 ? AnimationType.CircleAnimation.rawValue : AnimationType.SliderAnimation.rawValue
        CustomUserDefaults.animationType = type
        ExeciseDao.setDuration(execise: execise ?? ExeciseObject(), duration: duration)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnThreeMinute(_ sender: Any) {
        durationIV.image = UIImage(named: "ThreeminutesImage")
        duration = 3
    }
    
    @IBAction func actionOnFiveMinute(_ sender: Any) {
        durationIV.image = UIImage(named: "fiveMinutesImage")
        duration = 5
    }
    
    func registerCell() {
        let circleAnimationCVC = UINib(nibName: "FilterAnimationCVC", bundle: nil)
        collectionView.register(circleAnimationCVC, forCellWithReuseIdentifier: "FilterAnimationCVC")
        
        let SilderAnimationCVC = UINib(nibName: "FilterSliderAnimationCVC", bundle: nil)
        collectionView.register(SilderAnimationCVC, forCellWithReuseIdentifier: "FilterSliderAnimationCVC")
    }
    
    func setUpNavigationBar() {
        navigationBarV.infoIV.isHidden = true
        navigationBarV.titleL.text = "Settings"
        navigationBarV.popViewController = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

extension FilterVC: UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterAnimationCVC", for: indexPath) as! FilterAnimationCVC
            cell.SetUpCircleAnimation()
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterSliderAnimationCVC", for: indexPath) as! FilterSliderAnimationCVC
            cell.setUpProgress()
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 240, height: 240)
    }
    
    
}


extension FilterVC: UIScrollViewDelegate {
    
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let width = scrollView.frame.width
    currentPage = Int(scrollView.contentOffset.x / width)
    pageController.currentPage = currentPage
  }
    
}

        /// SPECIFIED CLASS FOR CUSTOMIZE UISWITCH HEIGHT AND WEIGHT

class SwitchBtn: UISwitch {
    
    override func draw(_ rect: CGRect) {
        let desiredWidth: CGFloat = 36
        let desiredHeight: CGFloat = 20
        
        let scaleX = desiredWidth / 51
        let scaleY = desiredHeight / 31
        
        self.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
    }
    
}
