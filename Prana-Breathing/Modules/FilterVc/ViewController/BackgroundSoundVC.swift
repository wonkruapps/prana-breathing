//
//  BackgrounSoundVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 02/12/23.
//

import Foundation
import UIKit
import AVFoundation

class BackgroundSoundVC: BaseVC {
    
    static let name = "BackgroundSoundVC"
    static let storyBoard = "BackgroundSound"
    
    class func instantiateFromStoryboard() -> BackgroundSoundVC {
        let vc = UIStoryboard(name: BackgroundSoundVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: BackgroundSoundVC.name) as! BackgroundSoundVC
        return vc
    }
    
    @IBOutlet var tableView: UITableView!
    
    var audioPlayer: AVAudioPlayer?
    var musicData = [MusicModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appendMusicData()
        registerCell()
    }
    
    @IBAction func actionOnDismiss() {
        dismiss(animated: true)
    }
    
    func registerCell() {
        let backgroundsoundTVC = UINib(nibName: "BackgroundSoundTVC", bundle: nil)
        tableView.register(backgroundsoundTVC, forCellReuseIdentifier: "BackgroundSoundTVC")
    }
    
    func appendMusicData() {
        musicData.removeAll()
        musicData.append(MusicModel(fileName: "Autumn-break" ,localName: "Autumn Break"))
        musicData.append(MusicModel(fileName: "Bamboo-grooves" ,localName: "Bamboo Grooves"))
        musicData.append(MusicModel(fileName: "Peaceful-rain" ,localName: "Peaceful rain"))
        musicData.append(MusicModel(fileName: "Garden-whispers" ,localName: "Garden Whispers"))
        musicData.append(MusicModel(fileName: "Harmony-waves" ,localName: "Harmony Waves"))
        musicData.append(MusicModel(fileName: "Pebble-shoreline",localName: "Pebble Shoreline"))
        musicData.append(MusicModel(fileName: "Summer-bliss" ,localName: "Summer Bliss"))
    }
    
    func playAudio(music: MusicModel) {
        guard let audioPath = Bundle.main.path(forResource: music.fileName, ofType: "mp3") else {
            print("Audio file not found")
            return
        }

        let audioURL = URL(fileURLWithPath: audioPath)

        do {
            audioPlayer = try AVAudioPlayer(contentsOf: audioURL)
            audioPlayer?.play()
        } catch {
            print("Error loading audio file: \(error.localizedDescription)")
        }
    }
    
}

extension BackgroundSoundVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musicData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BackgroundSoundTVC", for: indexPath) as! BackgroundSoundTVC
        cell.configCell(music: musicData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        musicData.forEach({ $0.isSelected = false })
        let data = musicData[indexPath.row]
        musicData[indexPath.row].isSelected = !data.isSelected
        tableView.reloadData()
        playAudio(music: data)
        CustomUserDefaults.setBackgroundMusic(data: data.fileName)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
