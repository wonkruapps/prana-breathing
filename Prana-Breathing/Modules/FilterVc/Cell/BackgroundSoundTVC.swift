//
//  BackgroundSoundTVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 01/12/23.
//

import UIKit

class BackgroundSoundTVC: UITableViewCell {
    
    @IBOutlet var checkBoxIV: UIImageView!
    @IBOutlet var fileNameL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(music: MusicModel) {
        checkBoxIV.image = music.isSelected ? UIImage.fillCheckBox : UIImage.emptyCheckBox
        fileNameL.text = music.localName
    }
    
}
