//
//  FilterSliderAnimationCVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 09/11/23.
//

import UIKit
import HGCircularSlider

class FilterSliderAnimationCVC: UICollectionViewCell {
    
    @IBOutlet var circleSilder: CircularSlider!
    @IBOutlet var progressInnerL: UILabel!
    
    var timer: Timer?

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpProgress()
    }
    
    func setUpProgress() {
        timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(updateSliderValue), userInfo: nil, repeats: true)
        circleSilder.endPointValue = 0
        progressInnerL.setCornerRadius(radius: 45)
    }
    
    @objc func updateSliderValue() {
        if circleSilder.endPointValue >= circleSilder.maximumValue {
            circleSilder.endPointValue = 0
            timer?.invalidate()
        } else {
            circleSilder.endPointValue += 0.3
        }
    }

}
