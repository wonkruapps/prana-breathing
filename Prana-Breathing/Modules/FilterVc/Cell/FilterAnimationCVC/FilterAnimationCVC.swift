//
//  FilterAnimationCVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 09/11/23.
//

import UIKit
import Lottie

class FilterAnimationCVC: UICollectionViewCell {
    
    @IBOutlet var animationV: UIView!
    @IBOutlet var animationBorberV: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        SetUpCircleAnimation()
    }
    
    func SetUpCircleAnimation() {
        let animationView = LottieAnimationView(animation: LottieAnimation.named(Utils.theme == 0 ? "BreathingAnimationLight" : "BreathingAnimationDark"))
        animationBorberV.setCornerRadius(radius: 105)
        animationView.loopMode = .playOnce
        animationView.animationSpeed = 0.3
        animationView.contentMode = .scaleToFill
        animationView.frame = CGRect(x: 0, y: 0, width: 240, height: 240)
        animationV.addSubview(animationView)
        
        let label = UILabel()
        label.text = ""
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(name: "Inter-Bold", size: 18)
        label.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        label.center = animationView.center
        animationV.addSubview(label)
        animationView.play()
    }

}
