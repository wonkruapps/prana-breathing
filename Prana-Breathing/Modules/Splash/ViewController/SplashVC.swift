//
//  SplashVC.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 08/11/23.
//

import Foundation
import UIKit
import Lottie

class SplashVC: UIViewController {
    static let name = "SplashVC"
    static let storyBoard = "Main"
    
    class func instantiateFromStoryboard() -> SplashVC {
        let vc = UIStoryboard(name: SplashVC.storyBoard, bundle: nil).instantiateViewController(withIdentifier: SplashVC.name) as! SplashVC
        return vc
    }
    
    @IBOutlet var animationV: UIView!
    
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUpCircleAnimation()
        timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(changeVC), userInfo: nil, repeats: false)
    }
    
    @objc func changeVC() {
        Utils.setRootVC(CustomUserDefaults.isOnboardFinished ? .homeVC : .onBoardVC)
    }
    
    func SetUpCircleAnimation() {
        let animationView = LottieAnimationView(animation: LottieAnimation.named("FlowerAnimation"))
        animationView.loopMode = .playOnce
        animationView.contentMode = .scaleToFill
        animationView.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        animationV.addSubview(animationView)
        animationView.play()
    }
}
