//
//  IAPManager.swift
//  GreenNoise
//
//  Created by Apzzo Technologies Private Limited on 04/06/23.
//
import Alamofire
import StoreKit
import UIKit
final class IAPManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    static let shared = IAPManager()

    var products = [SKProduct]()

    enum Product: String, CaseIterable {
        case yearly = "com.paced.breathing.yearly"
        case weekly = "com.paced.breathing.weekly"
    }

    public func fetchProducts() {
        if SKPaymentQueue.canMakePayments() {
            let set: Set<String> = [Product.yearly.rawValue,Product.weekly.rawValue]
            let productRequest = SKProductsRequest(productIdentifiers: set)
            productRequest.delegate = self
            productRequest.start()
        }
    }

    /// StoreKit delegate function
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
        for invalidIdentifier in response.invalidProductIdentifiers {
            // Handle any invalid product identifiers.
            print("Checking")
            print(invalidIdentifier)
        }
    }

    /// StoreKit when user complete the payment that time to call this function
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
                case .purchasing:
                    print("Customer is in the process the purchase")
                    NotificationCenter.default.post(name: .message, object: "Customer is in the process the purchase \n \n  \(transaction.error?.localizedDescription ?? "")")
                case .purchased:
                    print("Purchased")
                    NotificationCenter.default.post(name: .inAppPurchasePurchased, object: "Success")
                    SKPaymentQueue.default().finishTransaction(transaction)
                    NotificationCenter.default.post(name: .message, object: "Success")
                    NotificationCenter.default.post(name: .message, object: "Success \n \n \(transaction.error?.localizedDescription ?? "")")
                case .failed:
                    print("Failed")
                    NotificationCenter.default.post(name: .inAppPurchaseFailed, object: "Success")
                    SKPaymentQueue.default().finishTransaction(transaction)
                    NotificationCenter.default.post(name: .message, object: "Failed \n \n \(transaction.error?.localizedDescription ?? "")")
                case .restored:
                    print("Restored")
                    SKPaymentQueue.default().finishTransaction(transaction)
                    NotificationCenter.default.post(name: .restorePurchase, object: "Restored")
                    NotificationCenter.default.post(name: .message, object: "Restored \n \n \(transaction.error?.localizedDescription ?? "")")
                case .deferred:
                    print("deferred")
                    NotificationCenter.default.post(name: .inAppPurchasePurchased, object: "deferred")
                    NotificationCenter.default.post(name: .message, object: "deferred \n \n \(transaction.error?.localizedDescription ?? "")")
                default:
                    NotificationCenter.default.post(name: .message, object: "Default \n \n \(transaction.error?.localizedDescription ?? "")")
                    break
            }
        }
    }

    func purchase(product: Product) {
        guard SKPaymentQueue.canMakePayments() else {
            NotificationCenter.default.post(name: .inAppPurchaseFailed, object: "Success")
            return
        }

        guard let storeKitProduct = products.first(where: { $0.productIdentifier == product.rawValue }) else {
            NotificationCenter.default.post(name: .inAppPurchaseFailed, object: "Success")
            return
        }

        let paymentRequest = SKPayment(product: storeKitProduct)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(paymentRequest)
    }

    func receiptValidation(completionHandler: @escaping (_ error: String?, _ response: NSDictionary?) -> Void) {
        // Get the receipt if it's available.
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
           FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                print(receiptData)

                let receiptString = receiptData.base64EncodedString(options: [])

                // Read receiptData.

                let requestContents: [String: Any] = [
                    "receipt-data": receiptString,
                    "password": "7d8923cc04f54d708b9d5a327db3ffbd",
                ]

                let appleServer = appStoreReceiptURL.lastPathComponent == "sandboxReceipt" ? "sandbox" : "buy"

                let stringURL = "https://\(appleServer).itunes.apple.com/verifyReceipt"

                print("Loading user receipt: \(stringURL)...")

                Alamofire.request(stringURL, method: .post, parameters: requestContents, encoding: JSONEncoding.default)
                    .responseJSON { [self] response in
                        if let value = response.result.value  as? NSDictionary {
                            print(value)
                            getExpirationDateFromResponse(value)
                            completionHandler(nil, value)
                        } else {
                            let error = "Receiving receipt from App Store failed: \(response.result)"
                            print(error)
                            completionHandler(error, nil)
                        }
                    }
            } catch {
                let error = "Couldn't read receipt data with error: " + error.localizedDescription
                print(error)
                completionHandler(error, nil)
            }
        } else {
            completionHandler("AppStoreReceiptURL Path Not Exist", nil)
        }
    }

    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) {
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            let receiptObj = receiptInfo as! [NSDictionary]
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"

            for data in receiptObj {
                if let expiresStringDate = data["expires_date"] as? String {
                    let expiresDate = formatter.date(from: expiresStringDate) ?? Date()
                    let currentTime = date.timeIntervalSince1970
                    let expireTime = expiresDate.timeIntervalSince1970
                    print("current Time \(currentTime)")
                    print("expires Time \(expireTime)")
                    print("current Date \(date)")
                    print("expires Date \(expiresDate)")
                    if currentTime < expireTime {
                        Utils.isSubscribed = true
                        print(Utils.isSubscribed)
                        break
                    }
                }
            }
        } else {
            Utils.isSubscribed = false
        }
    }

    func restorePurchase() {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }

    func priceStringForProduct(_ item: SKProduct) -> String? {
        let price = item.price
        let numberFormatter = NumberFormatter()
        let locale = item.priceLocale
        print(locale)
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = .current
        return numberFormatter.string(from: price)
    }
}

