//
//  Notification+Extension.swift
//  GreenNoise
//
//  Created by Apzzo Technologies Private Limited on 04/06/23.
//

import UserNotifications

extension Notification.Name {
    static let restorePurchase = Notification.Name("Restore_Purchase")
    static let inAppPurchasePurchased = Notification.Name("InApp_Purchase_Purchased")
    static let inAppPurchaseFailed = Notification.Name("InApp_Purchase_Failed")
    static let message = Notification.Name("Warning_Message")
    static let applicationDidBecomeActive = Notification.Name("APP_DID_BECOME_ACTIVE")
}
