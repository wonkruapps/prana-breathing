//
//  UIView + Extension.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 03/11/23.
//

import Foundation
import UIKit

extension UIView {
    
    func setCornerRadius(radius: CGFloat, borderColor: UIColor = .clear, borderWidth: CGFloat = 1.5) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    func setBorderColor(borderColor: UIColor = .clear) {
        self.layer.borderColor = borderColor.cgColor
    }
    
}
