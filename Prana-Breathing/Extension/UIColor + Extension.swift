//
//  UIColor + Extension.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 03/11/23.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(hexStr: String) {
        var str = hexStr.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (str.hasPrefix("#")) {
            str = (str as NSString).substring(from: 1)
        }

        if (str.count != 6) {
            self.init(white: 0.5, alpha: 1)
            return
        }

        let rString = (str as NSString).substring(to: 2)
        let gString = ((str as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((str as NSString).substring(from: 4) as NSString).substring(to: 2)

        var r: CUnsignedInt = 0, g: CUnsignedInt = 0, b: CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)

        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    static var randomColor: UIColor {
        return .init(hue: .random(in: 0 ... 256), saturation: 0.6, brightness: 0.5, alpha: 1)
    }
    
    static var primaryColor: UIColor? { return UIColor(named: "PrimaryColor") }
    static var primaryTextColor: UIColor? { return UIColor(named: "PrimayTextColor") }
    static var SecondaryTextColor: UIColor? { return UIColor(named: "SecondaryTextColor") }
    static var darkModePrimary: UIColor? { return UIColor(named: "darkModePrimaryColor") }
    
}
