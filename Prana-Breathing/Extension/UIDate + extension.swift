//
//  UIDate + extension.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 16/11/23.
//

import Foundation
import UIKit

extension Date {
    
    func getFormattedDay() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E"
        let currentDate = Date()
        return dateFormatter.string(from: currentDate)
    }
    
    func getFormattedTime() -> String {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.string(from: currentDate)
    }
    
    func getFormattedDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let currentDate = Date()
        return dateFormatter.string(from: currentDate)
    }
}
