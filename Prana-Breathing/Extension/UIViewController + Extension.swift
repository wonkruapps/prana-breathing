//
//  UIViewController + Extension.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 07/11/23.
//

import Foundation
import UIKit

extension UIViewController {
    func setUpTitle(title: String) {
        navigationController?.navigationBar.isHidden = false
        navigationItem.title = title
        navigationItem.largeTitleDisplayMode = .always
    }
}
