//
//  Enumeration.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 06/11/23.
//

import Foundation
import UIKit

enum ProfileType {
    case maximumExhale
    case breathHold
}

enum RootVCType {
    case splashVC
    case onBoardVC
    case homeVC
    case profileVC
}

enum ExeciseType {
    case Relax
    case Calm
    case Balance
    case Strengthen
    case Focus
    case Energise
    case PacedBreathing
    
    var rawValue: String {
        switch self {
        case .Relax: return "Relax"
        case .Calm: return "Calm"
        case .Balance: return "Balance"
        case .Strengthen: return "Strengthen"
        case .Focus: return "Focus"
        case .Energise: return "Energise"
        case .PacedBreathing: return "PacedBreathing"
        }
    }
}


enum AnimationType {
    case CircleAnimation
    case SliderAnimation
    
    var rawValue: String {
        switch self {
            
        case .CircleAnimation:
            return "CircleAnimation"
        case .SliderAnimation:
            return "SliderAnimation"
        }
    }
}

enum Emoji {
    case Awful
    case Bad
    case Meh
    case Good
    case Great
    
    var rawValue: String {
        switch self {
        case .Awful:
            return "awfulEmoji"
        case .Bad:
            return "badEmoji"
        case .Meh:
            return "mehEmoji"
        case .Good:
            return "goodEmoji"
        case .Great:
            return "greatEmoji"
        }
    }
}
