//
//  Constants.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 30/11/23.
//

import Foundation


let PRIVACY_POLICY = "https://wonkru.co/app-privacy/"
let TERMS_AND_CONDITION = "https://wonkru.co/terms/"

let NOTIFICATION_IDENTIFIER = "NOTIFICATION_IDENTIFIER"
