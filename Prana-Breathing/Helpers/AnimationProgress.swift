//
//  AnimationProgress.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 22/11/23.
//

import Foundation
import UIKit

class CircularProgressBar: UIView {
    private let progressLayer = CAShapeLayer()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupProgressLayer()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupProgressLayer()
    }

    private func setupProgressLayer() {
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let radius = min(bounds.width, bounds.height) / 2.0

        let circularPath = UIBezierPath(arcCenter: center, radius: radius, startAngle: -CGFloat.pi / 2, endAngle: 3 * CGFloat.pi / 2, clockwise: true)

        progressLayer.path = circularPath.cgPath
        progressLayer.strokeColor = Utils.theme == 0 ? UIColor(hexStr: "97D8B2").cgColor : UIColor(hexStr: "16C75F").cgColor
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineWidth = 5.0
        progressLayer.strokeEnd = 0.0
        
        // Create a layer for the unfilled portion
        let unfilledLayer = CAShapeLayer()
        unfilledLayer.path = circularPath.cgPath
        unfilledLayer.strokeColor = Utils.theme == 0 ? UIColor(hexStr: "E6E6E6").cgColor : UIColor(hexStr: "585874").cgColor
        unfilledLayer.fillColor = UIColor.clear.cgColor
        unfilledLayer.lineWidth = 5.0
        unfilledLayer.strokeEnd = 1.0

        layer.addSublayer(unfilledLayer)
        layer.addSublayer(progressLayer)
    }

    func setProgress(progress: CGFloat,_  duration: Double = 1.0) {
        let animationKey = "progressAnimation"
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = progressLayer.strokeEnd
        animation.toValue = progress
        animation.duration = duration
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        progressLayer.strokeEnd = progress
        progressLayer.add(animation, forKey: animationKey)
    }
    
}
