//
//  CustomUserDefaults.swift
//  Random Timer
//
//  Created by Apzzo Technologies Private Limited on 05/07/23.
//

import UIKit

let ONBOARD_FINISHED = "ONBOARD_FINISHED"
let LOGIN_FINISHED = "LOGIN_FINISHED"
let ANIMATION_TYPE = "ANIMATION_TYPE"
let MAXIMUM_BREATHHOLD = "MAXIMUM_BREATHHOLD"
let MAXIMUM_EXHALE = "MAXIMUM_EXHALE"
let STEAK_LAST_DAY = "STEAK_LAST_DAY"
let CURRENT_STREAK = "CURRENT_STREAK"
let TOTAL_SESSION = "TOTAL_SESSION"
let TOTAL_MINUTES = "TOTAL_MINUTES"
let HOME_TITLE = "HOME_TITLE"
let USER_NAME = "USER_NAME"
let NOTIFICATION_ON = "NOTIFICATION_ON"
let VIBRATION_ON = "VIBRATION_ON"
let BACKGROUND_SOUND = "BACKGROUND_SOUND"
let BACKGROUND_ISON = "BACKGROUND_ISON"
let LAST_ACTIVE_DATE = "LAST_ACTIVE_DATE"
let NOTIFICATION_SCHEDULE_TIME = "NOTIFICATION_SCHEDULE_TIME"

class CustomUserDefaults: NSObject {
    
    static var isOnboardFinished: Bool {
        get {
            UserDefaults.standard.value(forKey: ONBOARD_FINISHED) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: ONBOARD_FINISHED)
        }
    }
    
    static var isLoggedIn: Bool {
        get {
            UserDefaults.standard.value(forKey: LOGIN_FINISHED) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: LOGIN_FINISHED)
        }
    }
    
    static var isNotificationOn: Bool {
        get {
            UserDefaults.standard.value(forKey: NOTIFICATION_ON) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: NOTIFICATION_ON)
        }
    }
    
    static var isVibrationOn: Bool {
        get {
            UserDefaults.standard.value(forKey: VIBRATION_ON) as? Bool ?? true
        }
        set {
            UserDefaults.standard.set(newValue, forKey: VIBRATION_ON)
        }
    }
    
    static var isBackgroundMusicOn: Bool {
        get {
            UserDefaults.standard.value(forKey: BACKGROUND_ISON) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: BACKGROUND_ISON)
        }
    }
    
    static var animationType: String {
        get {
            UserDefaults.standard.value(forKey: ANIMATION_TYPE) as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: ANIMATION_TYPE)
        }
    }
    
    static var userName: String {
        get {
            UserDefaults.standard.value(forKey: USER_NAME) as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: USER_NAME   )
        }
    }
    
    static var maximumBreathHold: Int {
        get {
            UserDefaults.standard.value(forKey: MAXIMUM_BREATHHOLD) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: MAXIMUM_BREATHHOLD)
        }
    }
    
    static var maximumExhale: Int {
        get {
            UserDefaults.standard.value(forKey: MAXIMUM_EXHALE) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: MAXIMUM_EXHALE)
        }
    }
    
    //MARK: - Set & Get Streak Last Day
    static func getStreakLastDay() -> Date? {
        return UserDefaults.standard.value(forKey: STEAK_LAST_DAY) as? Date ?? Calendar.current.date(byAdding: .day, value: -1, to: Date())
    }
    
    static func setStreakLastDay(data: Date) {
        UserDefaults.standard.set(data, forKey: STEAK_LAST_DAY)
    }
    
    //MARK: - Set & Get Current Streak
    static func getCurrentStreak() -> Int {
        return UserDefaults.standard.value(forKey: CURRENT_STREAK) as? Int ?? 0
    }
    
    static func setCurrentStreak(data: Int) {
        UserDefaults.standard.set(data, forKey: CURRENT_STREAK)
    }
    
    //MARK: - Set & Get Total Session
    static func getTotalSession() -> Int {
        return UserDefaults.standard.value(forKey: TOTAL_SESSION) as? Int ?? 0
    }
    
    static func setTotalSession(data: Int) {
        UserDefaults.standard.set(data, forKey: TOTAL_SESSION)
    }
    
    //MARK: - Set & Get Total Minutes
    static func getTotalMinute() -> Int {
        return UserDefaults.standard.value(forKey: TOTAL_MINUTES) as? Int ?? 0
    }
    
    static func setTotalMinute(data: Int) {
        UserDefaults.standard.set(data, forKey: TOTAL_MINUTES)
    }
    
    //MARK: - Set & Get Home title string
    static func getTitleString() -> String {
        return UserDefaults.standard.value(forKey: HOME_TITLE) as? String ?? ""
    }
    
    static func setTitleString(data: String) {
        UserDefaults.standard.set(data, forKey: HOME_TITLE)
    }
    
    //MARK: - Set & Get Background Sound
    static func getBackgroundMusic() -> String? {
        return UserDefaults.standard.value(forKey: BACKGROUND_SOUND) as? String ?? ""
    }
    
    static func setBackgroundMusic(data: String) {
        UserDefaults.standard.set(data, forKey: BACKGROUND_SOUND)
    }
    //MARK: - Set & Get Last Active Day
    static func getLastActiveDay() -> Date? {
        return UserDefaults.standard.value(forKey: LAST_ACTIVE_DATE) as? Date ?? Calendar.current.date(byAdding: .day, value: -1, to: Date())
    }
    
    static func setLastActiveDay(data: Date) {
        UserDefaults.standard.set(data, forKey: LAST_ACTIVE_DATE)
    }
    
    //MARK: - Set & Get Notificarion Schedule Time
    static func getNotificatonScheduleTime() -> [String] {
        return UserDefaults.standard.value(forKey: NOTIFICATION_SCHEDULE_TIME) as? [String] ?? []
    }
    
    static func setNotificatonScheduleTime(data: [String]) {
        UserDefaults.standard.set(data, forKey: NOTIFICATION_SCHEDULE_TIME)
    }
}

