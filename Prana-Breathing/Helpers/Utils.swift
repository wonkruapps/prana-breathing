//
//  Utils.swift
//  GreenNoise
//
//  Created by Apzzo Technologies on 22/05/23.
//

import UIKit
import MBProgressHUD

class Utils: NSObject {
    
    
    static var theme = 0
    static var isSubscribed = false
    static var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    
    // MARK: Other Methods
    /// It will redirect to the user particular root view controller
    /// - Parameter rootVCType: RootVCType
    class func setRootVC(_ rootVCType: RootVCType ) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let splashVC = SplashVC.instantiateFromStoryboard()
        let pranaTabBarController = PranaTabBarController.instantiateFromStoryboard()
        let profileVC = ProfileVC.instantiateFromStoryboard()
        let onBoardVC = OnBoardingNewVC.instantiateFromStoryboard()
        
        switch rootVCType {
        case .splashVC:
            appDelegate.window?.rootViewController = CustomNavigationController(rootViewController: splashVC)
        case .profileVC:
            UIApplication.shared.windows.first?.rootViewController = CustomNavigationController(rootViewController: profileVC)
        case .homeVC:
            UIApplication.shared.windows.first?.rootViewController = CustomNavigationController(rootViewController: pranaTabBarController)
        case .onBoardVC:
            UIApplication.shared.windows.first?.rootViewController = CustomNavigationController(rootViewController: onBoardVC)
        }
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    
    // MARK: - Show & Hide HUD

    class func showHUD(view: UIView) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading..."
    }

    class func hideHUD(view: UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }

    class func showAlert(title: String = "", message: String, viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        let OKAction = UIAlertAction(title: "Ok", style: .default) { _ in}
        alertController.addAction(OKAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    class func attributedText() -> NSMutableAttributedString {
        let fullText = "By continuing, you agree to Paced Breathing’s Terms & Conditions and Privacy Policy."

                let attributedString = NSMutableAttributedString(string: fullText)

                // Find the range of "Terms & Conditions" in the full text
                if let termsRange = fullText.range(of: "Terms & Conditions") {
                    let nsRange = NSRange(termsRange, in: fullText)
                    attributedString.addAttribute(.foregroundColor, value: UIColor.darkModePrimary!, range: nsRange)
                }

                // Find the range of "Privacy Policy" in the full text
                if let privacyRange = fullText.range(of: "Privacy Policy") {
                    let nsRange = NSRange(privacyRange, in: fullText)
                    attributedString.addAttribute(.foregroundColor, value: UIColor.darkModePrimary!, range: nsRange)
                }

                // Apply the attributed string to the label
                return attributedString
    }
    
    class func get24HourTime(time: String) -> String {
        let dateAsString = time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "HH"
        let Date24 = dateFormatter.string(from: date!)
        return Date24
    }
    
    class func get24HourMinute(time: String) -> String {
        let dateAsString = time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "mm"
        let Date24 = dateFormatter.string(from: date!)
        return Date24
    }
}
