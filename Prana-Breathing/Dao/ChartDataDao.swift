//
//  ChartDataDao.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 28/11/23.
//

import Foundation
import RealmSwift

class ChartDataDao: Object {
    @Persisted var dummyProperty: String = ""
    
    static let realm = try! Realm(configuration: Realm.Configuration(deleteRealmIfMigrationNeeded: true))
    
    class func saveOrUpdateUser(chartData: ChartDataModel) {
        try! realm.write {
            realm.add(chartData, update: .all)
        }
    }
    
    class func getAllChartData() -> [ChartDataModel] {
        return Array(realm.objects(ChartDataModel.self))
    }
    
    class func saveOrUpdateEmoji(chartData: [ChartDataModel],emoji: String) {
        try! realm.write {
            chartData.forEach({ $0.emotion = emoji })
        }
    }
}
