//
//  DailyHabitDao.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 23/11/23.
//

import Foundation
import RealmSwift

class DailyHabitDao: Object {
    @Persisted var dummyProperty: String = ""
    
    static let realm = try! Realm(configuration: Realm.Configuration(deleteRealmIfMigrationNeeded: true))
    
    class func saveOrUpdateUser(habit: DailyHabit) {
        let existingData = getAllHabits().filter({ $0.breathingType == habit.breathingType && $0.day == habit.day })
        if !existingData.isEmpty {
            try! realm.write {
                realm.delete(existingData)
            }
        }
        try! realm.write {
            realm.add(habit, update: .all)
        }
    }
    
    class func getAllHabits() -> [DailyHabit] {
        return Array(realm.objects(DailyHabit.self))
    }
    
    class func getHabitsByDay(day: String) -> [DailyHabit] {
        return Array(realm.objects(DailyHabit.self)).filter({ $0.day == day })
    }
    
    class func deleteHabit(habit: DailyHabit) {
        try! realm.write {
            realm.delete(habit)
        }
    }
    
    class func taskStatusChange(habit: DailyHabit){
        try! realm.write {
            habit.time = "Task Completed..."
        }
    }
}
