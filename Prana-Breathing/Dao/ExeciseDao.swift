//
//  ExeciseDao.swift
//  Prana-Breathing
//
//  Created by Apzzo Technologies Private Limited on 23/11/23.
//

import Foundation
import RealmSwift

class ExeciseDao: Object {
    @Persisted var dummyProperty: String = ""
    
    static let realm = try! Realm(configuration: Realm.Configuration(deleteRealmIfMigrationNeeded: true))
    
    class func saveOrUpdateUser(execise: [ExeciseObject]) {
        try! realm.write {
            realm.add(execise, update: .all)
        }
    }
    
    class func getAllExecise() -> [ExeciseObject] {
        return Array(realm.objects(ExeciseObject.self))
    }
    
    class func setDuration(execise: ExeciseObject, duration: Int) {
        try! realm.write {
            execise.duration = duration
        }
    }
    
    class func getSingleExecise(breathType: String) -> ExeciseObject {
        return realm.objects(ExeciseObject.self).first(where: { $0.breathingType == breathType }) ?? ExeciseObject()
    }
}
